// this manages dialogue for the game
// can edit in godot to alter UI elements etc
// e.g. matching textures to different characters if want to show the character face
using Godot;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

public class DialogueControl : Control
{
	// the different characters that are possible
	public enum CharacterEnum { Queenie, Freddie, Marky, Paul  }
	public string PlayerName {get; set;} = "Mr No-name";

	// Signal emitted during dialogue as appropriate
	// a string entry is then added to the player's journal
	// to update the journal:
	// EmitSignal(nameof(JournalUpdated), "jouirnal text");
	[Signal]
	public delegate void JournalUpdated(string journalUpdated);

	// when it is appropriate for the companion to say something, emit this signal:
	// EmitSignal(nameof(CompanionReacts), Companion.CompanionLine.Greeting1); 
	// (replace Greeting1 with the appropriate line. you can customise this in Companion.cs (ctrl + p -> Companion.cs))
	[Signal]
	public delegate void CompanionReacts(string line, AudioData.CompanionSounds sound);
	
	// when the dialogue option should affect the heartrate, emit this signal
	// EmitSignal(nameof(HeartRateModified), -5)
	// e.g. this reduces the heart rate by 5 (if the dialogue had a calming effect)
	[Signal]
	public delegate void HeartRateModified(int amount);

	public string JournalEntry;
	public bool Finished = false;
	public int DialogueIndex = 0;
	public int DialogueSize;
	RichTextLabel TextPanel; 
	Label NPCName; 
	string NPCNameString;
	Tween Tween;
	public Dictionary<int, string> DialogueDisplayDict = new Dictionary <int, string>();
	public Dictionary<Enum, List<string>> CharacterDataDict = new Dictionary<Enum, List<string>>();
	public List<string> _queenieList = new List<string>();
	public List<string> _freddieList = new List<string>();
	public List<string> _markyList = new List<string>();
	public List<string> _paulList = new List<string>();
	Random random = new Random();
	public InkStory inkStory;
	public bool AreThereChoices = false;
	public int n = 0;
	public int numOfChoices = 0;
	public Dictionary<int, string> ChoiceDict = new Dictionary <int, string>();
	public Button button1;
	public Button button2;
	public Button button3;
	Sprite Portrait;
	string _queeniePath;
	string _markyPath;
	string _freddiePath;
	string _paulPath;
	Texture PortraitTextureResource;
	RichTextLabel NextLabel;
	public string JournalText;
	public string ConversationUnlockedText;
	public int ChangeInHeartRate;

	// reference to dialoguecontrol sound player:
	private AudioStreamPlayer _soundPlayer;

	public List<string> HRChangeTriggerHistorylist = new List<string>();
	private Dictionary<AudioData.DialogueSound, AudioStream> _dialogueSounds = AudioData.LoadedSounds<AudioData.DialogueSound>(AudioData.DialogueSoundPaths);
//	private Dictionary<AudioData.CompanionSounds, AudioStream> _companion = AudioData.LoadedSounds<AudioData.CompanionSounds>(AudioData.CompanionSoundPaths);
	private Dictionary<string, AudioData.CompanionSounds> _companionDialogues = new Dictionary<string, AudioData.CompanionSounds>();

	private Dictionary<string, AudioData.DialogueSound> _dialogues = new Dictionary<string, AudioData.DialogueSound>()
	{
		// {"test", AudioData.DialogueSound.JackIntro} // this is a placeholder!
	};

	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		GetNode<MarginContainer>("CanvasLayer/MarginContainer").Visible = false;// Hide(); 
		TextPanel = (RichTextLabel)GetNode("CanvasLayer/MarginContainer/TextureRect/VBoxContainer/PanelContainer/MarginContainer3/TextPanel");
		Tween = (Tween)GetNode("Tween");
		Portrait = (Sprite)GetNode("CanvasLayer/MarginContainer/TextureRect/VBoxContainer/HBoxContainer/HBoxContainer2/PanelContainer2/Sprite");
		NPCName = (Label)GetNode("CanvasLayer/MarginContainer/TextureRect/VBoxContainer/HBoxContainer/HBoxContainer2/PanelContainer/NameLabel");
		button1 = (Button)GetNode("CanvasLayer/MarginContainer/TextureRect/VBoxContainer/VBoxContainer/VButtonContainer/Button1");
		button2 = (Button)GetNode("CanvasLayer/MarginContainer/TextureRect/VBoxContainer/VBoxContainer/VButtonContainer/Button2");
		button3 = (Button)GetNode("CanvasLayer/MarginContainer/TextureRect/VBoxContainer/VBoxContainer/VButtonContainer/Button3");
		NextLabel = (RichTextLabel)GetNode("CanvasLayer/MarginContainer/NextControl/NextLabel");
		MakeCharacterDictionaries();	//stores the character enums with the correspondning names and path to portrait sprite	
		//UpdatePlayerName("Test!");
		LoadInkStory(); //loads the inkdata
		MakeCompanionDialogueSoundDictionary();
	}

	public void MakeCompanionDialogueSoundDictionary()
	{
		_companionDialogues.Add("Psst. We really should talk to peculiar Paul, down there. He knows more, I can feel it in my ventricles.", AudioData.CompanionSounds.Paul); //replace test1 with PaulUnlocked audio
		_companionDialogues.Add("Psst. Hate to say this, love, but we really need to talk to Wacko Marko at reception, right?", AudioData.CompanionSounds.Mark); //replace test2 with MarkUnlocked audio
	}
	

	public void PlayDialogueSound(string inkyString)
	{
		AudioHandler.PlaySound(_soundPlayer,_dialogueSounds[_dialogues[inkyString]], AudioData.SoundBus.Voice);
	}

	public void CheckForJournalUpdates() //Checks for journal text updates and emits signals
	{
		if (JournalText != (string)inkStory.GetVariable("journal_text")) //Check that the journal text variable is not the same as the ink journal text variable because this would have already been transmitted
		{
			JournalText = (string)inkStory.GetVariable("journal_text"); //update the variable to the inkjournaltext variabke
			//GD.Print(JournalText + " newjournaltextdetected signal emitted");
			EmitSignal(nameof(JournalUpdated), JournalText);
		}
		return;
	}
	public void CheckForUnlockedConversations() //Checks for journal text updates and emits signals
	{
		if (ConversationUnlockedText != (string)inkStory.GetVariable("conversation_unlocked") ) 
		{
			ConversationUnlockedText = (string)inkStory.GetVariable("conversation_unlocked"); 

			if (ConversationUnlockedText != "TBC")
			{
				EmitSignal(nameof(CompanionReacts), ConversationUnlockedText, _companionDialogues[ConversationUnlockedText]);
			} 
		}
		return;
	}
	
	public void CheckForHeartRateUpdates(string buttonText) //checks for heart rate changes and emits a signal
	{
		if ((bool)inkStory.GetVariable("heart_rate_updated") == true) //starts off as false and resets to false each time a new HRChange is transmitted through signal
		{
			GD.Print("***");
			foreach (string blah in HRChangeTriggerHistorylist)
			{
				GD.Print(blah);
			}
			GD.Print("***");
			if (HRChangeTriggerHistorylist.Contains(buttonText) == false)
			{
				ChangeInHeartRate = (int)inkStory.GetVariable("heart_rate_change"); //set a new change in heart rate
				EmitSignal(nameof(HeartRateModified), ChangeInHeartRate); //pass the new change in HR through the signal
				inkStory.SetVariable("heart_rate_updated", false); //reset the variables
				inkStory.SetVariable("heart_rate_change", 0); //reset the variable
				GD.Print(ChangeInHeartRate + "change_in_HR"); // testing
			}
		}
		return;
	}

	public void MakeCharacterDictionaries() //store in a dictionary: Enum,CharacterDataList (List contains: Name, path to portrait)
	{
		_queeniePath = "res://Actors/Thumbs/Queenie.png";
		_markyPath = "res://Actors/Thumbs/Marky.png";
		_freddiePath = "res://Actors/Thumbs/Freddie.png";
		_paulPath = "res://Actors/Thumbs/Paul.png";
		_markyList.Add("Marky");
		_markyList.Add(_markyPath);
		_queenieList.Add("Queenie");
		_queenieList.Add(_queeniePath);
		_freddieList.Add("Freddie");
		_freddieList.Add(_freddiePath);
		_paulList.Add("Paul");
		_paulList.Add(_paulPath);
		CharacterDataDict.Add(CharacterEnum.Queenie, _queenieList); //Queenie
		CharacterDataDict.Add(CharacterEnum.Marky, _markyList); //Marky
		CharacterDataDict.Add(CharacterEnum.Freddie, _freddieList); //Freddie
		CharacterDataDict.Add(CharacterEnum.Paul , _paulList); //Paul
		//so, CharacterDataDict[characterenum] = charlist
		//CharacterDataDict[characterenum][0] = charactername string
		//CharacterDataDict[characterenum][1] = string path to the sprite
	}
	public void LoadInkStory() //simply loads the ink file
	{
		inkStory = GetNode<InkStory>("Ink Story");
		inkStory.InkFile = GD.Load("res://Systems/Dialogue/TillDeathDoUsHeart.json");
		inkStory.LoadStory();
		UpdatePlayerName(PlayerName);
		//PlayerName = "Sarah"; //for testing
	}
	public void UpdatePlayerName(string newName) //Check if this still works - seems fine in test but need to check.
	{
		PlayerName = newName;
		inkStory.SetVariable("player_name", PlayerName); //Update the name to the player's input name. 
	}

	public override void _Process(float delta)
	{
		GetNode<Button>("CanvasLayer/MarginContainer/TextureRect/VBoxContainer/VBoxContainer/BtnContinue").Visible = Finished;// NextLabel.Visible = Finished;
		// if (Input.IsActionJustPressed("interact"))
		// {
		// 	LoadTextPanelDialogue(); //Load the next bit of dialogue whenever the player presses E
		// }	

	}

	// called when a character is clicked
	public void Start(CharacterEnum character)
	{
		GetTree().Paused = true;
		//GD.Print(character.ToString() + " dialogue begin...");
		UpdateCharacterNameAndPortrait(character);
		//Looks up the story that is stored under the characters name: (NPCNameString)
		if (inkStory.ChoosePathString(NPCNameString))
			{
				MakeDialogueDictForDisplay(); //stores the strings so they can be displayed gradually
			}
	}

	public void UpdateCharacterNameAndPortrait(CharacterEnum character)
	{
		string spritepath = CharacterDataDict[character][1];
		PortraitTextureResource = (Texture)GD.Load(spritepath);
		NPCNameString = CharacterDataDict[character][0];
	}

	private void OnInkStoryContinued(string text, string tags)
	{
		// Replace with function body.
	}


	public void MakeDialogueDictForDisplay() //stores the dialogue on separate lines so that it can be displayed gradually
	{
		DialogueIndex = 0;
		DialogueDisplayDict.Clear(); //don't clear the dictionary if it is a loop
		n = -1;
		while (inkStory.CanContinue)
		{
		//	GD.Print("While loop started");
			n = n+1;
		//	GD.Print (n + "n value");
			string text = inkStory.Continue();
			DialogueDisplayDict.Add(n,text);
		//	GD.Print(text);
		}
		LoadTextPanelDialogue();
	}

	public void LoadTextPanelDialogue() //Shows the dialogue but hides the buttons at first
	{
		this.Show();
		GetNode<MarginContainer>("CanvasLayer/MarginContainer").Visible = true;
		DialogueSize = DialogueDisplayDict.Count;
	//	GD.Print(DialogueSize + "Dialog size");
	//	GD.Print(DialogueIndex + "dialogue index");
		button1.Hide();
		button2.Hide();
		button3.Hide();
		
		if(DialogueIndex < DialogueSize) //uses the dialogue size to show the dialogue line by line
		{
			this.Show();
			GetNode<MarginContainer>("CanvasLayer/MarginContainer").Visible = true;
			Finished = false;
			SetNameAndPortrait();
			TextPanel.BbcodeText = DialogueDisplayDict[DialogueIndex]; 
			TextPanel.PercentVisible = 0;
			Tween.InterpolateProperty(TextPanel,"percent_visible", 0, 1, 1, Tween.TransitionType.Linear, Tween.EaseType.InOut);
			Tween.Start();
			DialogueIndex += 1;
		}
		else //when we reach the end of the main initial dialogue, we ask ink if there are any choices available. If so, we show the buttons.
		{
			if (inkStory.HasChoices)
			{
				button1.Show();
				button2.Show();
				button3.Show();
			}
			else
			{
				{
					button1.Hide();
					button2.Hide();
					button3.Hide();
					GetNode<MarginContainer>("CanvasLayer/MarginContainer").Visible = false;
					GetTree().Paused = false;
				}
			}
			
		}
	}
	
	public void OnHUDPause(bool enabled)
	{
		button1.Disabled = button2.Disabled = button3.Disabled = enabled;
		if (GetNode<MarginContainer>("CanvasLayer/MarginContainer").Visible)
		{
			GetTree().Paused = true;
		}
	}

	public void SetNameAndPortrait()
	{
		NPCName.Text = NPCNameString; 
		Portrait.Scale = new Vector2(0.2133f, 0.2133f);
		Portrait.Texture = PortraitTextureResource;
		
	}


	private void OnInkStoryChoicesAvailable(string choices) //don't actually use this here because we always have 3 choices
	{

		if (inkStory.HasChoices)
			{
				ChoiceDict.Clear();
				numOfChoices = -1;
				AreThereChoices = true;
				foreach (string c in inkStory.CurrentChoices)
				{
					numOfChoices ++;
					ChoiceDict.Add(numOfChoices, c);
				}
				button1.GetNode<Label>("Label").Text = ChoiceDict[0];
				if (numOfChoices>0)
				{
					button2.GetNode<Label>("Label").Text = ChoiceDict[1];
				}
				if (numOfChoices>1)
				{
					button3.GetNode<Label>("Label").Text = ChoiceDict[2];
				}
				if (numOfChoices==1) 
				{
					button3.Hide();
				}
				if (numOfChoices==0)
				{
					button2.Hide();
					button3.Hide();
				}			
			} 
	}

	private void OnTextTweenCompleted(Godot.Object @object, NodePath key)
	{
		Finished = true;
	}

	private void OnButton1Pressed() //tells the button which inkstory text to display, then the response is turned into a dialogue dictionary. Every time we press the button, we check whether our choice has changed any of the variables
	{
		inkStory.ChooseChoiceIndex(0);
		while (inkStory.CanContinue)
		{
			MakeDialogueDictForDisplay();		
		}
		CheckForJournalUpdates();
		CheckForHeartRateUpdates(button1.GetNode<Label>("Label").Text);
		CheckForUnlockedConversations();
		AddToHRHistoryList(button1.GetNode<Label>("Label").Text);
	}

	private void AddToHRHistoryList(string text)
	{
		if (!HRChangeTriggerHistorylist.Contains(text))
		{
			HRChangeTriggerHistorylist.Add(text);
		}
	}

	private void OnButton2Pressed()
	{
		inkStory.ChooseChoiceIndex(1);
		while (inkStory.CanContinue)
		{
			MakeDialogueDictForDisplay();		
		}
		CheckForJournalUpdates();
		CheckForHeartRateUpdates(button2.GetNode<Label>("Label").Text);
		CheckForUnlockedConversations();
		AddToHRHistoryList(button2.GetNode<Label>("Label").Text);
	}

	private void OnButton3Pressed()
	{
		inkStory.ChooseChoiceIndex(2);
		while (inkStory.CanContinue)
		{
			MakeDialogueDictForDisplay();		
		}
		CheckForJournalUpdates();
		CheckForHeartRateUpdates(button3.GetNode<Label>("Label").Text);
		CheckForUnlockedConversations();
		AddToHRHistoryList(button3.GetNode<Label>("Label").Text);
	}

///////////////////////All for testing:
	private void OnSamButtonPressed()
	{
		Start(CharacterEnum.Marky);
	}

	private void OnJackButtonPressed()
	{
		Start(CharacterEnum.Queenie);
	}

	private void OnDanielButtonPressed()
	{
		Start(CharacterEnum.Freddie);
	}

	private void OnPaulButtonPressed()
	{
		Start(CharacterEnum.Paul);
	}


	private void OnBtnContinuePressed()
	{
		LoadTextPanelDialogue();
	}

}












