// TurnManager. this is to remind me how to use this
// ***
// First initialise the turn manager by making a list of units and passing them into the constructor
// If you want to do something at the end of each round, connect the RoundFinished signal
// Do "_turnmanager.NextTurn()" to start
// Do things for the current turn using _turnmanager.CurrentUnit
// When the unit turn is exhausted (e.g. after an action, or after pressing "End Turn", or after action points are exhuasted), do _turnmanager.EndTurn()
// You can do anything you like in between turns. When you are ready to go to the next turn, do _turnmanager.NextTurn()
// Keep track of the current round by accessing _turnManager.CurrentRound
// ***
// You must implement the OnStartTurn and OnEndTurn methods (as well as Initiative) for your unit. 
// The example shows how to use them to alter appearance during the turn (showing panel behind active unit).
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class TurnManager : Reference
{
	[Signal]
	public delegate void RoundFinished();

	public int CurrentRound {get; set;} = 1;

	private List<ITurnBasedUnit> _remainingUnits;
	private List<ITurnBasedUnit> _finishedUnits = new List<ITurnBasedUnit>();
	public ITurnBasedUnit CurrentUnit {get; private set;}
	public TurnManager()
	{
		NotImplementedException e = new NotImplementedException();
		GD.Print(e.Message);
		throw e;
	}
	public TurnManager(List<ITurnBasedUnit> unitList)
	{
		_remainingUnits = unitList;
	}

	public void SortUnitList()
	{
		_remainingUnits.Sort((x, y) => y.Initiative.CompareTo(x.Initiative));

		// foreach (ITurnBasedUnit unit in _remainingUnits)
		// {
		// 	GD.Print(unit.Initiative);
		// }
	}

	public void EndTurn()
	{
		CurrentUnit.OnEndTurn();
		_remainingUnits.Remove(CurrentUnit);
		_finishedUnits.Add(CurrentUnit);
		if (_remainingUnits.Count == 0)
		{
			EmitSignal(nameof(RoundFinished));
			NewRound();
		}
	}

	private void NewRound()
	{
		GD.Print("new round.");
		CurrentRound += 1;
		_remainingUnits = _finishedUnits.ToList();
		_finishedUnits.Clear();
		SortUnitList();
	}

	public void NextTurn()
	{
		CurrentUnit = _remainingUnits[0];
		CurrentUnit.OnStartTurn();
	}
}
