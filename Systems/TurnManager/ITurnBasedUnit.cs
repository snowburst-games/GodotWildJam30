using Godot;
using System;

public interface ITurnBasedUnit
{
	float Initiative {get;}

	void OnStartTurn();

	void OnEndTurn();
}
