using Godot;
using System;
using System.Collections.Generic;

public class BtnBase : Button
{
	private AudioStreamPlayer _soundPlayer;
	private Dictionary<AudioData.ButtonSounds, AudioStream> _btnSounds = AudioData.LoadedSounds<AudioData.ButtonSounds>(AudioData.ButtonSoundPaths);
	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }

	private void OnBtnMouseEntered()
	{
		AudioHandler.PlaySound(_soundPlayer,_btnSounds[AudioData.ButtonSounds.Hover], AudioData.SoundBus.Effects);
	}

	private void OnBtnPressed()
	{
		AudioHandler.PlaySound(_soundPlayer,_btnSounds[AudioData.ButtonSounds.Click], AudioData.SoundBus.Effects);
	}

}

