using Godot;
using System;

public class HUD : CanvasLayer
{
	private Button _btnJournal;
	private Button _btnSubmitName;
	private Button _btnJournalBack;
	private Button _btnMenu;
	private Panel _pnlMenu;
	private PnlSettings _pnlSettings;
	private PackedScene _scnLblFloatScore;

	[Signal]
	public delegate void HUDPause(bool enabled);

	private Panel _pnlPlayerName;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_scnLblFloatScore = GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.LblFloatScore]);
		_btnJournal = GetNode<Button>("Panel/BtnJournal");
		_btnMenu = GetNode<Button>("Panel/BtnMenu");
		_btnSubmitName = GetNode<Button>("Panel/PnlPlayerName/BtnSubmitName");
		_btnJournalBack = GetNode<Button>("Panel/PnlJournal/BtnBack");
		_pnlMenu = GetNode<Panel>("Panel/PnlMenu");
		_pnlSettings = GetNode<PnlSettings>("Panel/PnlSettings");
		_pnlPlayerName = GetNode<Panel>("Panel/PnlPlayerName");
		_pnlMenu.Visible = _pnlSettings.Visible = false;
		

	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		// SpawnLblFLoatScore(-5);
	}
	public void SpawnLblFloatScore(int modifier)
	{
		GD.Print("spawning");
		LblFloatScore lblFloatScore = (LblFloatScore) _scnLblFloatScore.Instance();
		AddChild(lblFloatScore);
		MoveChild(lblFloatScore, GetChildCount()-2);
		lblFloatScore.Text = modifier < 0 ? "-" + modifier : "+" + modifier;
		lblFloatScore.RectGlobalPosition = GetNode<BadHeart>("Panel/BadHeart").GlobalPosition;
	}

	private void OnBtnMenuPressed()
	{
		PauseAllButOneButton(null);
		// GetTree().Paused = true;
		// _btnJournal.Disabled = _btnMenu.Disabled = _btnSubmitName.Disabled = _btnJournalBack.Disabled = true;
		_pnlMenu.Visible = true;
		// Replace with function body.
	}

	private void OnBtnSubmitNamePressed()
	{
		GetTree().Paused = false;
		_pnlPlayerName.Visible = false;
		_btnJournal.Disabled = _btnMenu.Disabled = _btnJournalBack.Disabled = false;
		EmitSignal(nameof(HUDPause), false);
	}

	private void PauseAllButOneButton(Button btn)
	{
		GetTree().Paused = true;
		_btnJournal.Disabled = _btnMenu.Disabled = _btnSubmitName.Disabled =  _btnJournalBack.Disabled = true;
		EmitSignal(nameof(HUDPause), true);
		if (btn == null)
		{
			return;
		}
		btn.Disabled = false;
	}

	public void ShowPlayerNamePanel()
	{
		PauseAllButOneButton(_btnSubmitName);
	}

	private void OnBtnJournalPressed()
	{
		PauseAllButOneButton(_btnJournalBack);
	}

	private void OnBtnMenuResumePressed()
	{
		GetTree().Paused = false;
		_pnlMenu.Visible = false;
		_btnJournal.Disabled = _btnMenu.Disabled = _btnSubmitName.Disabled = _btnJournalBack.Disabled = false;
		EmitSignal(nameof(HUDPause), false);
	}


	private void OnBtnSettingsPressed()
	{
		_pnlSettings.Visible = true;
	}


}
