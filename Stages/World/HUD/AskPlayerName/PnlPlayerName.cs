using Godot;
using System;

public class PnlPlayerName : Panel
{
	[Signal]
	public delegate void NameSubmitted(string name);

	public override void _Ready()
	{
		Visible = true;
		
	}
	private void OnBtnSubmitNamePressed()
	{
		GetTree().Paused = false;
		Visible = false;
		EmitSignal(nameof(NameSubmitted), GetNode<LineEdit>("LEdAskName").Text);
	}


}

