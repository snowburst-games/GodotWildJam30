using Godot;
using System;
using System.Collections.Generic;

public class PnlJournal : Panel
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private Label _lblBody;

	private List<string> _entries = new List<string>();

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_lblBody = GetNode<Label>("LblBody");
		_lblBody.Text = "";
		Visible = false;
	}


	public void OnJournalUpdated(string entry)
	{
		if (_entries.Contains(entry))
		{
			return;
		}
		_entries.Add(entry);
		_lblBody.Text += ("\n" + entry);
	}

	private void OnBtnJournalPressed()
	{
		Visible = true;
	}


	private void OnBtnBackPressed()
	{
		Visible = false;
	}

	// public override void _Input(InputEvent ev)
	// {
	// 	if (Visible && ev is InputEventMouseButton evMouseButton && ev.IsPressed())
	// 	{
	// 		if (! (evMouseButton.Position.x > RectGlobalPosition.x && evMouseButton.Position.x < RectSize.x + RectGlobalPosition.x
	// 		&& evMouseButton.Position.y > RectGlobalPosition.y && evMouseButton.Position.y < RectSize.y + RectGlobalPosition.y) )
	// 		{
	// 			OnBtnBackPressed();
	// 		}
	// 	}
	// }

}

