using Godot;
using System;

public class HeartLblFloatSpawn : Position2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private PackedScene _scnLblFloatScore;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_scnLblFloatScore = GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.LblFloatScore]);
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
	public void SpawnLblFLoatScore(int modifier)
	{
		// GD.Print("spawning");
		LblFloatScore lblFloatScore = (LblFloatScore) _scnLblFloatScore.Instance();
		lblFloatScore.Text = modifier < 0 ? "-" + modifier : "+" + modifier;
		lblFloatScore.RectGlobalPosition = this.GlobalPosition;
		AddChild(lblFloatScore);
	}
}
