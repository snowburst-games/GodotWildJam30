using Godot;
using System;
using System.Collections.Generic;

public class StageWorld : Stage
{

	private DialogueControl _dialogueControl;
	private StoryManager _storyManager;
	private LevelManager _levelManager;
	private PnlJournal _pnlJournal;
	private Companion _companion;
	private BadHeart _badHeart;
	private Random _rand = new Random();
	// private HeartLblFloatSpawn _heartLblSpawnPoint;
	private HUD _HUD;
	private MusicPlayer _musicPlayer;
	private AudioStreamPlayer _soundPlayer;
	private Dictionary<AudioData.WorldSounds, AudioStream> _worldSounds = AudioData.LoadedSounds<AudioData.WorldSounds>(AudioData.WorldSoundPaths);

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		base._Ready();
		_badHeart = GetNode<BadHeart>("HUD/Panel/BadHeart");
		_HUD = GetNode<HUD>("HUD");
		// _heartLblSpawnPoint = GetNode<HeartLblFloatSpawn>("HUD/HeartLblFloatSpawn");
		_musicPlayer = GetNode<MusicPlayer>("MusicPlayer");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_dialogueControl = GetNode<DialogueControl>("LevelManager/DialogueControl");
		_pnlJournal = GetNode<PnlJournal>("HUD/Panel/PnlJournal");
		_storyManager = GetNode<StoryManager>("HUD/StoryManager");
		_levelManager = GetNode<LevelManager>("LevelManager");
		_companion = GetNode<Companion>("HUD/Panel/Companion");
		_levelManager.Connect(nameof(LevelManager.HeartRateModified), _badHeart, nameof(BadHeart.OnHeartRateChanged));
		_levelManager.Connect(nameof(LevelManager.HeartRateModified), _HUD, nameof(HUD.SpawnLblFloatScore));
		// _levelManager.Connect(nameof(LevelManager.HeartRateModified), this, nameof(OnHeartRateModified));
		_levelManager.Connect(nameof(LevelManager.HeartFound), _storyManager, nameof(StoryManager.StartVictorySequence));
		_levelManager.Connect(nameof(LevelManager.HeartFound), this, nameof(OnVictory));
		_dialogueControl.Connect(nameof(DialogueControl.JournalUpdated), _pnlJournal, nameof(PnlJournal.OnJournalUpdated));
		_dialogueControl.Connect(nameof(DialogueControl.CompanionReacts), _companion, nameof(Companion.StartSpeech));
		_levelManager.Connect(nameof(DialogueControl.CompanionReacts), _companion, nameof(Companion.StartSpeech));
		_levelManager.Connect(nameof(LevelManager.MiniGameShown), this, nameof(OnMiniGameShown));
		_dialogueControl.Connect(nameof(DialogueControl.HeartRateModified), _badHeart, nameof(BadHeart.OnHeartRateChanged));
		_dialogueControl.Connect(nameof(DialogueControl.HeartRateModified), _HUD, nameof(HUD.SpawnLblFloatScore));
		_badHeart.Connect(nameof(BadHeart.MaxHeartRateExceeded), _storyManager, nameof(StoryManager.StartDefeatSequence));
		_badHeart.Connect(nameof(BadHeart.MaxHeartRateExceeded), this, nameof(OnMaxHeartRateExceeded));
		_badHeart.Connect(nameof(BadHeart.HeartRateChanged), this, nameof(OnHeartRateChanged));
		_HUD.Connect(nameof(HUD.HUDPause), _dialogueControl, nameof(DialogueControl.OnHUDPause));
		GetNode<PnlPlayerName>("HUD/Panel/PnlPlayerName").Connect(nameof(PnlPlayerName.NameSubmitted), _dialogueControl, nameof(DialogueControl.UpdatePlayerName));
		GetNode<PnlPlayerName>("HUD/Panel/PnlPlayerName").Connect(nameof(PnlPlayerName.NameSubmitted), _levelManager, nameof(LevelManager.UpdatePlayerName));
		GetNode<PnlPlayerName>("HUD/Panel/PnlPlayerName").Connect(nameof(PnlPlayerName.NameSubmitted), this, nameof(OnNameSubmitted));
		_storyManager.EndStoryFinished+=GoToMainMenu;
		GetNode<Control>("HUD/Panel/CntConfirmExit").Visible = false;
		
		// _companion.StartSpeech(Companion.CompanionLine.Greeting1);
		// _companion.StartSpeech("aaaaaaaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaa fhgfhgfhgf hgfhgfhg fhgf rthrthrthgfhgfhgf hgfhgf hgf hgfh gfh fg hgf hgf hgfh gfh fg hfg hgfh gf hgf hgf", AudioData.CompanionSounds.Test1);
		// AudioHandler.PlaySound(_soundPlayer,_worldSounds[AudioData.WorldSounds.Win],AudioData.SoundBus.Voice);

		// _storyManager.OnCharacterAccused(DialogueControl.CharacterEnum.Dwight, false); // testing only
	}

	
	public override void Init()
	{
		base.Init();
		// GetTree().Paused = true;
		_HUD.ShowPlayerNamePanel();
		// _musicPlayer.Pl
		AudioHandler.PlaySound(_musicPlayer, _worldSounds[AudioData.WorldSounds.Music], AudioData.SoundBus.Music);
		
		// OnHeartRateChanged(60);
	}


	public void OnNameSubmitted(string name)
	{
		CompanionIntro();
	}

	private async void CompanionIntro()
	{
		_companion.StartSpeech("So you're the human they've lumbered me with. This ain't no place for an artichoke heart.", AudioData.CompanionSounds.SoYouHuman);

		await ToSignal(_companion.GetNode<AnimationPlayer>("Anim"), "animation_finished");
		_companion.StartSpeech("Oof. It's stuffy in here - 'urry up and find yer heart so I can go back to the open air.", AudioData.CompanionSounds.Oof);
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }

	private void OnMaxHeartRateExceeded()
	{
		_companion.Stop();
		_musicPlayer.FadeThenStop();	
	}

	public void OnVictory()
	{
		_musicPlayer.FadeThenStop();
	}

	private void OnHeartRateChanged(int newHeartRate)
	{
		
		_companion.OnHeartRateChanged(newHeartRate, _dialogueControl.PlayerName);
		_musicPlayer.PitchScale = newHeartRate/60f;
		GD.Print(_musicPlayer.PitchScale);
		int busEffectsIndex = AudioServer.GetBusIndex(AudioData.SoundBusStrings[AudioData.SoundBus.Music]);
		int numberofEffects = AudioServer.GetBusEffectCount(busEffectsIndex);
		for (int i = 0; i < numberofEffects; i++)
		{
			AudioServer.RemoveBusEffect(busEffectsIndex, i);
		}
		AudioEffectPitchShift audioEffect = new AudioEffectPitchShift();
		audioEffect.PitchScale = 1 / _musicPlayer.PitchScale;
		AudioServer.AddBusEffect(busEffectsIndex, audioEffect);
	}

	public void OnMiniGameShown()
	{
		if (!_levelManager.GetNode("MiniGame").IsConnected(nameof(MiniGame.MiniGameClosed), this, nameof(OnMiniGameClosed)))
		{
			_levelManager.GetNode("MiniGame").Connect(nameof(MiniGame.MiniGameClosed), this, nameof(OnMiniGameClosed));
		}
		_musicPlayer.PauseAndPlayNext(_worldSounds[AudioData.WorldSounds.MinigameMusic]);
		// _musicPlayer.FadeThenPause();
		// // if (_musicPlayer.PausedMusic.ContainsKey(_worldSounds[AudioData.WorldSounds.MinigameMusic]))
		// // {
		// 	_musicPlayer.ResumeStream(_worldSounds[AudioData.WorldSounds.MinigameMusic]);
		// }
		// else
		// {

		// 	// AudioHandler.PlaySound(_musicPlayer, _worldSounds[AudioData.WorldSounds.MinigameMusic], AudioData.SoundBus.Music);
		// }
	}

	public void OnMiniGameClosed()
	{
		_musicPlayer.PauseAndPlayNext(_worldSounds[AudioData.WorldSounds.Music]);
		// if (_musicPlayer.PausedMusic.ContainsKey(_worldSounds[AudioData.WorldSounds.MinigameMusic]))
		// {
			// _musicPlayer.FadeThenPause();
			// _musicPlayer.ResumeStream(_worldSounds[AudioData.WorldSounds.Music]);
		// }
		// else
		// {
		// 	AudioHandler.PlaySound(_musicPlayer, _worldSounds[AudioData.WorldSounds.Music], AudioData.SoundBus.Music);
		// }
	}
	public void DoPaperSound()
	{
		AudioHandler.PlaySound(_soundPlayer, _rand.Next(0,2) == 0 ? _worldSounds[AudioData.WorldSounds.Paper] : _worldSounds[AudioData.WorldSounds.Paper2], AudioData.SoundBus.Effects);
	}

	public void DoAccuseSound()
	{
		AudioHandler.PlaySound(_soundPlayer, _worldSounds[AudioData.WorldSounds.Accuse], AudioData.SoundBus.Effects);
	}

	public void GoToMainMenu()
	{
		_HUD.GetNode<Panel>("Panel").Visible = false;
		GetNode<Control>("HUD/Panel/CntConfirmExit").Visible = false;
		// _dialogueControl.Die();
		_storyManager.Die();
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}
	private void OnBtnMainMenuPressed()
	{
		GetNode<Control>("HUD/Panel/CntConfirmExit").Visible = true;
	}
	private void OnConfirmExitBtnNoPressed()
	{
		GetNode<Control>("HUD/Panel/CntConfirmExit").Visible = false;
	}
	private void OnBtnQuitPressed()
	{
		GoToMainMenu();
	}

}



