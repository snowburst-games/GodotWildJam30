using Godot;
using System;
using System.Collections.Generic;

public class StoryManager : Control
{

	public delegate void EndStoryFinishedDelegate();
	public event EndStoryFinishedDelegate EndStoryFinished;

	private PictureStory _pictureStory;

	private Dictionary<AudioData.WorldSounds, AudioStream> _worldSounds = AudioData.LoadedSounds<AudioData.WorldSounds>(AudioData.WorldSoundPaths);

	private Dictionary<DialogueControl.CharacterEnum, string> _endTextDict = new Dictionary<DialogueControl.CharacterEnum, string>()
	{
		// { DialogueControl.CharacterEnum.Mimi, @"Mimi: ""It wasn't me! You're my dear friend! I would never steal your sandwich. Okay... I lied. I lied about where I was at the time of the crime. I had to lie... what would I do if people found out the truth? I was with Tobias. We're more than just friends you see...""" },
		// { DialogueControl.CharacterEnum.Glenn, @"Glenn: ""Yes, you've found me out. I don't like my broccoli brownies. They're disgusting! Broccoli is not a dessert! After raving about my new recipe for so long, I was too embarrassed to admit my mistake. And when you walked in to the office that morning, with that beautiful sandwich glistening in your lunch bag... I knew I had to have it! But I was too late. By the time I snuck into the kitchen, the fridge was empty. The sandwich was gone!""" },
		// { DialogueControl.CharacterEnum.Tobias, @"Tobias: ""Look. I don't like you. I can never forgive you for constantly beating me at Tic Tac Toe... But I would never do such a dastardly deed as steal someone's sandwich! Besides, I was with Mimi. I lied because... no-one can find out that Mimi and I... we're more than friends.""" },
		// { DialogueControl.CharacterEnum.Stan, @"Stan: ""You cannot have been more wrong. I didn't steal your sandwich. I despise thieves. Walk to the kitchen? I never leave my seat! Even going to the toilet is a waste of time - yes, I do what astronauts do. Maximum Absorbancy Garment. Look it up. It'll change your life.""" },
		// { DialogueControl.CharacterEnum.Robin, @"Robin: ""You win. How clever of you. Yes, I took your sandwich. It was the only way I could get you to notice me! I've been dead for days... A ghost. My murderer works among us. No matter how many staplers I hid, I couldn't get you to see me. It was only when I took your sandwich that you saw the poor bludgeoned ghost girl. And now you're going to help me get my revenge...""" }
	};

	private Dictionary<DialogueControl.CharacterEnum, string> _endImgDict = new Dictionary<DialogueControl.CharacterEnum, string>()
	{
		// { DialogueControl.CharacterEnum.Glenn, "res://Stages/World/Story/DefeatPictureStory.png" },
		// { DialogueControl.CharacterEnum.Mimi, "res://Stages/World/Story/DefeatPictureStory.png" },
		// { DialogueControl.CharacterEnum.Robin, "res://Stages/World/Story/VictoryPictureStory.png" },
		// { DialogueControl.CharacterEnum.Stan, "res://Stages/World/Story/DefeatPictureStory.png" },
		// { DialogueControl.CharacterEnum.Tobias, "res://Stages/World/Story/DefeatPictureStory.png" }
	};


	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_pictureStory = GetNode<PictureStory>("PictureStory");
		// StartVictorySequence();
	}

	public void OnCharacterAccused(DialogueControl.CharacterEnum character, bool guilty)
	{
		// if (guilty)
		// {
		// 	StartVictorySequence(character);
		// 	return;
		// }
		// StartDefeatSequence(character);
		
	}

	// public void StartDefeatSequence(DialogueControl.CharacterEnum character)
	// {

	// 	_pictureStory.Reset();
	// 	_pictureStory.PictureStoryFinished+=this.OnEndStoryFinished;
	// 	_pictureStory.StayPausedOnFinish = true;
	// 	_pictureStory.FadeOutOnFinish = false;
	// 	_pictureStory.VisibleOnFinish = true;
	// 	_pictureStory.AddScreen(_endImgDict[character], _endTextDict[character]);
	// 	_pictureStory.Start();
	// }
	public void StartDefeatSequence()
		{

			_pictureStory.Reset();
			_pictureStory.PictureStoryFinished+=this.OnEndStoryFinished;
			_pictureStory.StayPausedOnFinish = true;
			_pictureStory.FadeOutOnFinish = false;
			_pictureStory.VisibleOnFinish = true;
			_pictureStory.AddScreen("res://Stages/World/Story/Defeat.png", "",
				_worldSounds[AudioData.WorldSounds.Lose]);
			_pictureStory.Start();
		}


	// public void StartVictorySequence(DialogueControl.CharacterEnum character)
	// {
	// 	_pictureStory.Reset();
	// 	_pictureStory.PictureStoryFinished+=this.OnEndStoryFinished;
	// 	_pictureStory.StayPausedOnFinish = true;
	// 	_pictureStory.FadeOutOnFinish = false;
	// 	_pictureStory.VisibleOnFinish = true;
	// 	_pictureStory.AddScreen(_endImgDict[character], _endTextDict[character], _worldSounds[AudioData.WorldSounds.Win]);
	// 	_pictureStory.OverrideColor(new Color(1,1,1), new Color(0,0,0));
	// 	_pictureStory.Start();
	// }
	public void StartVictorySequence()
	{
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnEndStoryFinished;
		_pictureStory.StayPausedOnFinish = true;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
			_pictureStory.AddScreen("res://Stages/World/Story/Victory.png", 
			"Queenie: \"Yes, I took your heart. D'you know why? You remember my show, don't you? The Mended Hearts Exhibit? It gave me more joy than any surgery ever could. But then, you had to write your little review. 'Not. Bad.' you said. You know how many hearts I snatched for my art? For a \"Not bad\"?! So, when you wondered into the hospital with a grazed knee, I couldn’t believe my luck. The next thing I knew: I had opened your chest and your heart was in my pocket.\"",
			_worldSounds[AudioData.WorldSounds.Win]);
		_pictureStory.OverrideColor(new Color(1,1,1), new Color(1,1,1));
		_pictureStory.Start();
	}


	public void OnEndStoryFinished()
	{
		EndStoryFinished?.Invoke();
	}

	public void Die()
	{
		EndStoryFinished = null;
	}

}
