// Stage menu: simple menu script connecting local and networked game signals to our scene manager.
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class StageMainMenu : Stage
{
	private AudioStreamPlayer _musicPlayer;
	private AudioStreamPlayer _soundPlayer;
	private Dictionary<AudioData.MainMenuSounds, AudioStream> _mainMenuSounds = AudioData.LoadedSounds<AudioData.MainMenuSounds>(AudioData.MainMenuSoundPaths);
	private PictureStory _pictureStory;
	private Panel _popAbout;
	private PnlSettings _pnlSettings;

	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);

	public override void _Ready()
	{
		base._Ready();
		// SetBtnToggleFullScreenText();
		_popAbout = GetNode<Panel>("PopAbout");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_pictureStory = GetNode<PictureStory>("PictureStory");
		_pnlSettings = GetNode<PnlSettings>("PnlSettings");
		_pnlSettings.Visible = false;

		if (OS.HasFeature("HTML5"))
		{
			GetNode<Button>("HBoxContainer/BtnQuit1").Visible = false;
			GetNode<Button>("HBoxContainer/BtnToggleMute").Visible = true;
			_pnlSettings.GetNode<CheckBox>("CntPanels/PnlGraphics/CBoxFullScreen").Visible = false;
			_pnlSettings.GetNode<Label>("CntPanels/PnlGraphics/LblTitle").Text = "Not available in HTML5 version.";
		}
		
		AudioHandler.PlaySound(_musicPlayer, _mainMenuSounds[AudioData.MainMenuSounds.Music], AudioData.SoundBus.Music);

		
		_popAbout.Visible = false;
		GetNode<Button>("HBoxContainer/BtnToggleMute").Text = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master")) ? "UNMUTE" : "MUTE";
		
		_musicPlayer.PitchScale = 1f;
		int busEffectsIndex = AudioServer.GetBusIndex(AudioData.SoundBusStrings[AudioData.SoundBus.Music]);
		int numberofEffects = AudioServer.GetBusEffectCount(busEffectsIndex);
		for (int i = 0; i < numberofEffects; i++)
		{
			AudioServer.RemoveBusEffect(busEffectsIndex, i);
		}
	}


	private void Intro() // this is the new game intro
	{
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnIntroFinished;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/INTRO_PICTURE_STORY.png", 
		@"Friday 14th of February 2021, 8.05pm.
Oh, they're waking up… ‘ello, love! You've just had surgery to mend your broken little heart. And you're being discharged! B-but before you go, there's something you should know: During the operation, we seem to have misplaced your heart, but not to worry! We've replaced it with a perfectly good artichoke heart. You'll never know the difference, e-except it can't handle excitement. Find your stolen heart. Keep your heartbeat below 200, otherwise, well, terrible things.", _mainMenuSounds[AudioData.MainMenuSounds.Intro]);
		_pictureStory.OverrideColor(new Color(1,1,1), new Color(1,1,1));
		_pictureStory.Start();
		
	}
	private void OnIntroFinished()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World);
	}

	private void OnBtnPlayPressed()
	{
		Intro();
	}

	private void OnBtnAboutPressed()
	{
		GetNode<Panel>("PopAbout").Visible = true;
		GetNode<Control>("PopAbout/CntAbout").Visible = true;
	}

	private void OnBtnBackPressed()
	{
		GetNode<Panel>("PopAbout").Visible = false;
	}

	public override void _Input(InputEvent ev)
	{
		if (_popAbout.Visible && ev is InputEventMouseButton evMouseButton && ev.IsPressed())
		{
			if (! (evMouseButton.Position.x > _popAbout.RectGlobalPosition.x && evMouseButton.Position.x < _popAbout.RectSize.x + _popAbout.RectGlobalPosition.x
			&& evMouseButton.Position.y > _popAbout.RectGlobalPosition.y && evMouseButton.Position.y < _popAbout.RectSize.y + _popAbout.RectGlobalPosition.y) )
			{
				OnBtnBackPressed();
			}
		}
	}

	private void _on_BtnQuit_pressed()
	{
		GetTree().Quit();
	}

	private void ToggleMute()
	{
		bool muted = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master"));
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Master"), !muted);
		string label = muted ? "MUTE" : "UNMUTE";
		GetNode<Button>("HBoxContainer/BtnToggleMute").Text = label;
	}

	private void OnGameLinkButtonPressed()
	{
		OS.ShellOpen("https://sage7.itch.io/");
	}


	private void OnBtnScoresPressed()
	{
		_pnlSettings.Visible = true;
	}

	private void OnBtnTexBackgroundPressed()
	{
		_pnlSettings.OnBtnClosePressed();
		_popAbout.Visible = false;
	}
}


