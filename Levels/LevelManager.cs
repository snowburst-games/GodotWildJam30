using Godot;
using System;
using System.Collections.Generic;

public class LevelManager : Control
{
	public enum Level { Reception, Ward, StaffRoom, Basement, Toilet, Garden }
	private Dictionary<Level, SceneData.Scene> _roomsDict = new Dictionary<Level, SceneData.Scene>() {
		{Level.Reception, SceneData.Scene.RoomReception},
		{Level.Ward, SceneData.Scene.RoomWard},
		{Level.StaffRoom, SceneData.Scene.RoomStaffRoom},
		{Level.Basement, SceneData.Scene.RoomBasement}
	};

	private string _playerName {get; set;} = "player name";

	private AnimationPlayer _anim;
	private TextureRect _texFade;

	[Export]
	private Level _startingLevel = Level.Reception;

	[Signal]
	public delegate void HeartRateModified(int modifier);

	[Signal]
	public delegate void HeartFound();

	[Signal]
	public delegate void CompanionReacts(string line, AudioData.CompanionSounds sound);

	private Room _currentLevel;
	private MiniGame _miniGame;

	private DialogueControl _dialogueControl;
	private PackedScene _scnMiniGame;

	private Dictionary<Level, string> _companionLinesPerLevel = new Dictionary<Level, string>()
	{
		// {Level.Ward, "blah blah blah"}
	};

	private Dictionary<Level, AudioData.CompanionSounds> _companionAudioPerLevel = new Dictionary<Level, AudioData.CompanionSounds>()
	{
		// {Level.Ward, AudioData.CompanionSounds.Test1}
	};

	[Signal]
	public delegate void MiniGameShown();


	public override void _Ready()
	{
		_anim = GetNode<AnimationPlayer>("Anim");
		_dialogueControl = GetNode<DialogueControl>("DialogueControl");
		_texFade = GetNode<TextureRect>("CanvasLayer/TexFade");
		_scnMiniGame = GD.Load<PackedScene>(SceneData.Scenes[SceneData.Scene.MiniGame]);
		_texFade.Visible = false;
		_miniGame = GetNode<MiniGame>("MiniGame");
		GetNode<TextureRect>("SplashMiniGame").Visible = false;

		SetLevel(_startingLevel);

		LaunchMiniGame(); // the minigame persists across rooms. any button that shows the minigame would need to be connected back to here (ShowMiniGame)
	}

	public async void SetLevelWithFade(Level level)
	{
		// if (!_roomsDict.ContainsKey(level))
		// {
		// 	return;
		// }
		_anim.Play("FadeOut");
		await ToSignal(_anim, "animation_finished");
		SetLevel(level);
		_anim.Play("FadeIn");
	}

	public void SetLevel(Level level)
	{
		if (_currentLevel != null)
		{
			_currentLevel.Die();
		}
		PackedScene _levelScn = GD.Load<PackedScene>(SceneData.Scenes[_roomsDict[level]]);
		_currentLevel = (Room) _levelScn.Instance();
		_currentLevel.Init();
		_currentLevel.DoorClicked+=this.SetLevelWithFade;

		Control cntInfoProps = _currentLevel.GetNode<Control>("CntInfoProps");

		foreach (Node n in cntInfoProps.GetChildren())
		{
			if (n.GetChildCount() > 0)
			{
				if (n.GetChild(0) is InfoProp infoProp)
				{
					infoProp.Connect(nameof(InfoProp.HeartRateModified), this, nameof(OnInfoPropHeartRateModified));
					infoProp.Connect(nameof(InfoProp.HeartFound), this, nameof(OnHeartFound));

					if (n.Name == "BtnMiniGame")
					{
						infoProp.Connect("pressed", this, nameof(ShowMiniGame));
					}
				}
			}
		}
		AddChild(_currentLevel);
		_currentLevel.CharacterManager.CharacterClicked+=_dialogueControl.Start;

		if (_companionLinesPerLevel.ContainsKey(level) && _companionAudioPerLevel.ContainsKey(level))
		{
			EmitSignal(nameof(CompanionReacts), _companionLinesPerLevel[level], _companionAudioPerLevel[level]);// _companionDialogues[ConversationUnlockedText]); UNCOMMENT THIS AFTER MERGE
		}

		if (_miniGame != null)
		{
			if (GetChildren().Contains(_miniGame))
			{
				MoveChild(_miniGame, _currentLevel.GetIndex());
				MoveChild(GetNode("SplashMiniGame"), _miniGame.GetIndex());
			}
		}

	}

	public void OnInfoPropHeartRateModified(int modifier)
	{	// GD.Print("emit a signal from lvemanager for heart rate modifier");
		EmitSignal(nameof(HeartRateModified), modifier);
	}

	public void OnHeartFound()
	{
		EmitSignal(nameof(HeartFound));
	}

	public void LaunchMiniGame()
	{
		// _miniGame = (MiniGame) _scnMiniGame.Instance();
		// _miniGame.Name = "MiniGame";
		// AddChild(_miniGame);
		// _miniGame.PlayArea.PlayerName = _playerName;
		_miniGame.Visible = false;
		_miniGame.PlayArea.Connect(nameof(PnlPlayArea.HeartRateModified), this, nameof(OnInfoPropHeartRateModified));
	}

	public void ShowMiniGame()
	{
		_miniGame.Visible = true;
		GetNode<AnimationPlayer>("AnimMiniGame").Play("Show");
		EmitSignal(nameof(LevelManager.MiniGameShown));
	}

	public void UpdatePlayerName(string name)
	{
		_playerName = name;
		_miniGame.PlayArea.PlayerName = _playerName;
	}
	private void OnMiniGameBtnClosedPressed()
	{
		GetNode<AnimationPlayer>("AnimMiniGame").Play("Hide");
	}
}


