using Godot;
using System;
using System.Collections.Generic;

public class Room : Control
{
	public delegate void DoorClickedDelegate(LevelManager.Level targetLevel);
	public event DoorClickedDelegate DoorClicked;
	private AudioStreamPlayer _soundPlayer;
	public CharacterManager CharacterManager {get; set;}
	private Control _cntDoors;
	private Control _cntInfoProps;
	private Dictionary<AudioData.RoomSounds, AudioStream> _roomSounds = AudioData.LoadedSounds<AudioData.RoomSounds>(AudioData.RoomSoundPaths);

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_cntDoors = GetNode<Control>("CntDoors");

		CharacterManager = GetNode<CharacterManager>("CharacterManager");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");

		foreach (Node n in _cntDoors.GetChildren())
		{
			if (n is Door door)
			{
				door.Connect("pressed", this, nameof(OnDoorClicked), new Godot.Collections.Array {door.TargetLevel, door});
			}
		}
	}

	public void OnDoorClicked(LevelManager.Level level, Door door)
	{
		// if (level != LevelManager.Level.HR || level != LevelManager.Level.Office)
		// {
		// 	return;
		// }
		DoorClicked?.Invoke(level);
		door.OnDoorOpened();
	}

	public void Init() // runs before _Ready
	{

	}

	public void Die()
	{
		DoorClicked = null;
		CharacterManager.Die();
		// null all events
		QueueFree();

	}
}


