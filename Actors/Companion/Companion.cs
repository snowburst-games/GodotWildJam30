using Godot;
using System;
using System.Collections.Generic;

public class Companion : Node2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private Sprite _spriteSpeechBubble;
	private Label _lblBubble;
	private Control _cntSpeechBubble;
	private AnimationPlayer _anim;
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.CompanionSounds, AudioStream> _companionSounds = AudioData.LoadedSounds<AudioData.CompanionSounds>(AudioData.CompanionSoundPaths);

	public enum CompanionLine {
		WellDone1,
		Greeting1,
		// HeartRate60,
		HeartRate90,
		HeartRate120,
		HeartRate150
	}

	private List<string> _linesSaid = new List<string>();

	private Dictionary<CompanionLine, AudioData.CompanionSounds> _companionLineSounds = new Dictionary<CompanionLine, AudioData.CompanionSounds>()
	{
		{CompanionLine.WellDone1, AudioData.CompanionSounds.Test1},
		{CompanionLine.Greeting1, AudioData.CompanionSounds.Test1},
		// {CompanionLine.HeartRate60, AudioData.CompanionSounds.Test1},
		{CompanionLine.HeartRate90, AudioData.CompanionSounds.CantGoOn},
		{CompanionLine.HeartRate120, AudioData.CompanionSounds.BeatBeatBeat},
		{CompanionLine.HeartRate150, AudioData.CompanionSounds.OneFiftyBeats}
	};

	private Dictionary<CompanionLine, string> _companionLineText = new Dictionary<CompanionLine, string>()
	{
		{CompanionLine.WellDone1, "well done, you clicked a button!!!1"},
		{CompanionLine.Greeting1, "greets done, you clicked a button!!!1"},
		// {CompanionLine.HeartRate60, "Remember, don't get me too excited. Two 'undred beats is my limit. Or else."},
		{CompanionLine.HeartRate90, "I can't go on. Let's play a little game, relax a little."},
		{CompanionLine.HeartRate120, "Beat, beat, beat. SO many beats. I'm a vegetable not an organ. Let me out!"},
		{CompanionLine.HeartRate150, "150 beats! Let's take it easy, play a little game, before I leave this wretched life."}
	};

	private Dictionary<int, CompanionLine> _heartRateLines = new Dictionary<int, CompanionLine>()
	{
		// {60, CompanionLine.HeartRate60 },
		{90, CompanionLine.HeartRate90 },
		{120, CompanionLine.HeartRate120 },
		{150, CompanionLine.HeartRate150 }
	};
		// if (newHeartRate > 60 )
		// {
		// 	_companion.StartSpeech("Remember, don't get me too excited. Two 'undred beats is my limit. Or else.", AudioData.CompanionSounds.Test1);
		// }
		// else if (newHeartRate > 90)
		// {
		// 	_companion.StartSpeech(string.Format("I can't go on. Let's play a little game, {0}, relax a little.", _dialogueControl.PlayerName), AudioData.CompanionSounds.Test1);
		// }
		// else if (newHeartRate > 120)
		// {
		// 	_companion.StartSpeech("Beat, beat, beat. SO many beats. I'm a vegetable not an organ. Let me out!", AudioData.CompanionSounds.Test1);
		// }
		// else if (newHeartRate >= 150)
		// {
		// 	_companion.StartSpeech("More than 150 beats! Let's take it easy, play a little game, before I leave this wretched life.", AudioData.CompanionSounds.Test1);
		// }	

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_cntSpeechBubble = GetNode<Control>("CntSpeechBubble");
		_lblBubble = _cntSpeechBubble.GetNode<Label>("LblBubble");
		_anim = GetNode<AnimationPlayer>("Anim");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		// _spriteSpeechBubble = GetNode<Sprite>("SpriteSpeechBubble");
		// _spriteSpeechBubble.Visible = false;
		_cntSpeechBubble.Visible = false;
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }


	public void OnHeartRateChanged(int newHeartRate, string playerName)
	{
		
		// if (newHeartRate > 60 && !_linesSaid.Contains(_companionLineText[line]))
		// {
		// 	StartSpeech(_companionLineText[line], _companionLineSounds[line]);
		// }
		if (newHeartRate > 90)
		{ 
			CompanionLine line = _heartRateLines[90];
			if (!_linesSaid.Contains(_companionLineText[line]))
			{
				StartSpeech(_companionLineText[line], _companionLineSounds[line]);
				return;
			}
		}	
		if (newHeartRate > 120)
		{ 
			CompanionLine line = _heartRateLines[120];
			if (!_linesSaid.Contains(_companionLineText[line]))
			{
				StartSpeech(_companionLineText[line], _companionLineSounds[line]);
				return;
			}
		}	
		if (newHeartRate > 150)
		{ 
			CompanionLine line = _heartRateLines[150];
			if (!_linesSaid.Contains(_companionLineText[line]))
			{
				StartSpeech(_companionLineText[line], _companionLineSounds[line]);
				return;
			}
		}	
		// if (newHeartRate > 60 )
		// {
		// 	_companion.StartSpeech("Remember, don't get me too excited. Two 'undred beats is my limit. Or else.", AudioData.CompanionSounds.Test1);
		// }
		// else if (newHeartRate > 90)
		// {
		// 	_companion.StartSpeech(string.Format("I can't go on. Let's play a little game, {0}, relax a little.", _dialogueControl.PlayerName), AudioData.CompanionSounds.Test1);
		// }
		// else if (newHeartRate > 120)
		// {
		// 	_companion.StartSpeech("Beat, beat, beat. SO many beats. I'm a vegetable not an organ. Let me out!", AudioData.CompanionSounds.Test1);
		// }
		// else if (newHeartRate >= 150)
		// {
		// 	_companion.StartSpeech("More than 150 beats! Let's take it easy, play a little game, before I leave this wretched life.", AudioData.CompanionSounds.Test1);
		// }	
	}

	public void StartSpeech(string line, AudioData.CompanionSounds sound)
	{
		if (_linesSaid.Contains(line))
		{
			return;
		}
		_linesSaid.Add(line);
		_lblBubble.Text = line;// _companionLineText[line];
		_lblBubble.RectSize = new Vector2(546,28);
		_anim.Stop();
		_anim.Play("FadeInOut");
		// AudioData.CompanionSounds sound = _companionLineSounds[line];
		AudioHandler.PlaySound(_soundPlayer, _companionSounds[sound], AudioData.SoundBus.Voice);		
	}

	public void Stop()
	{
		_anim.Stop();
		GetNode<Sprite>("CntSpeechBubble/SpriteSpeechBubble").Visible = false;
		GetNode<Sprite>("CntSpeechBubble/SpriteSpeechBubble").Modulate = new Color(1,1,1,0);
		_soundPlayer.Stop();
	}

}
