using Godot;
using System;
using System.Collections.Generic;

public class Character : TextureButton
{

	[Export]
	public DialogueControl.CharacterEnum CharacterType = DialogueControl.CharacterEnum.Marky;
	[Export]
	private string _infoText = "";

	// private Light2D _light;
	private Sprite _sprite;
	private Label _lbl;
	private InfoPanel _infoPanel;
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.CharacterSounds, AudioStream> _characterSounds = AudioData.LoadedSounds<AudioData.CharacterSounds>(AudioData.CharacterSoundPaths);

	private Func<AudioData.CharacterSounds> _GetRandomGreetingSound;

	private Dictionary<DialogueControl.CharacterEnum, Vector2> _characterTexDict = new Dictionary<DialogueControl.CharacterEnum, Vector2>()
	{
		// {DialogueControl.CharacterEnum.Glenn, new Vector2(0,0)},	// this only sorts position
		// {DialogueControl.CharacterEnum.Mimi, new Vector2(0,10)},
		// {DialogueControl.CharacterEnum.Tobias, new Vector2(10,10)},
		// {DialogueControl.CharacterEnum.Stan, new Vector2(10,20)},
		// {DialogueControl.CharacterEnum.Robin, new Vector2(10,30)}
	};

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_sprite = GetNode<Sprite>("Sprite");
		// _light = GetNode<Light2D>("Light2D");
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_lbl = GetNode<Label>("Label");
		_infoPanel = GetNode<InfoPanel>("InfoPanel");
		if (CharacterType == DialogueControl.CharacterEnum.Marky || CharacterType == DialogueControl.CharacterEnum.Paul)
		{
			_GetRandomGreetingSound = GetRandomMaleGreetingSound;	
		}
		else
		{
			_GetRandomGreetingSound = GetRandomFemaleGreetingSound;
		}
		// _light.Visible = false;
		// SetTexture();
		_lbl.Text = CharacterType.ToString();
		_infoPanel.Init(_infoText);
	}

	public void SetTexture()
	{
		_sprite.RegionRect = new Rect2(_characterTexDict[CharacterType], _sprite.RegionRect.Size);
	}

	private void OnMouseEntered()
	{
		// _light.Visible = true;
		Modulate = new Color(1.2f,1.2f,1.2f);
	}


	private void OnMouseExited()
	{
		// _light.Visible = false;
		Modulate = new Color(1,1,1);
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
	private void OnBtnSpriteGuiInput(InputEvent ev)
	{
		if (ev is InputEventMouseButton evb)
		{
			if (evb.Pressed)
			{
				if (evb.ButtonIndex == (int)ButtonList.Right)
				{
					_infoPanel.Init(_infoText);
					_infoPanel.Start();
					_infoPanel.Visible = true;
				}
			}
			else
			{
				_infoPanel.Visible = false;
			}
		}
		// GD.Print("test");
		// ((ShaderMaterial)Material).SetShaderParam("enabled", true);
	}



	private void OnCharacterPressed()
	{
		AudioHandler.PlaySound(_soundPlayer, _characterSounds[_GetRandomGreetingSound()], AudioData.SoundBus.Voice);
	}

	public AudioData.CharacterSounds GetRandomMaleGreetingSound()
	{
		return (new AudioData.CharacterSounds[3] {AudioData.CharacterSounds.MaleGreeting1, AudioData.CharacterSounds.MaleGreeting2,
			AudioData.CharacterSounds.MaleGreeting3 })
			[new Random().Next(0,3)];
	}
	public AudioData.CharacterSounds GetRandomFemaleGreetingSound()
	{
		return (new AudioData.CharacterSounds[3] {AudioData.CharacterSounds.FemaleGreeting1, AudioData.CharacterSounds.FemaleGreeting2,
			AudioData.CharacterSounds.FemaleGreeting3 })
			[new Random().Next(0,3)];
	}
}

