using Godot;
using System;

public class CharacterManager : Control
{

	public delegate void CharacterClickedDelegate(DialogueControl.CharacterEnum characterType);
	public event CharacterClickedDelegate CharacterClicked;

	// private DialogueControl _dialogueControl;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		// _dialogueControl = GetNode<DialogueControl>("DialogueControl");
		InitCharacters();
	}

	public void InitCharacters()
	{
		foreach (Node n in GetChildren())
		{
			if (n is Character c)
			{
				c.Connect("pressed", this, nameof(OnCharacterClicked), new Godot.Collections.Array {c.CharacterType});
			}
		}
	}

	public void OnCharacterClicked(DialogueControl.CharacterEnum characterType)
	{
		CharacterClicked?.Invoke(characterType);
		// _dialogueControl.Start(characterType);
	}

	public void Die()
	{
		CharacterClicked = null;
	}

}
