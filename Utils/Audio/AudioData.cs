// Audio data: contains references to all audio files in the project for convenience
// Contains method for loading required sounds - should load all sounds required for a stage at the start.
using System;
using System.Collections.Generic;
using Godot;

public static class AudioData
{

	public enum SoundBus {
		Voice,
		Effects,
		Music,
		Master,
		BadHeart
	}

	public static Dictionary<SoundBus, string> SoundBusStrings = new Dictionary<SoundBus, string>()
	{
		{SoundBus.Voice, "Voice"},
		{SoundBus.Effects, "Effects"},
		{SoundBus.Music, "Music"},
		{SoundBus.Master, "Master"},
		{SoundBus.BadHeart, "BadHeart"}
	};

	public enum MainMenuSounds {
		Music,
		Intro
	};

	public enum ButtonSounds {
		Hover,
		Click
	};

	public enum WorldSounds {
		Music,
		Paper,
		Paper2,
		Accuse,
		Win,
		MinigameMusic,
		Lose
	};

	public enum RoomSounds {
		
	};

	public enum DoorSound {
		Open,
		Door2,
		Door,
		Hole,
		Hole2
	}
	public enum NoughtsSounds {
		Start,
		End,
		CheatVictory
	}

	public enum MiniGameSound {
		HeartBolt,
		Transfusion,
		Sedation,
		Delirium,
		MagicalBarrier,
		ArcaneLaw,
		GreySoul,
		Plague,
		Recuperate,
		FleshWound,
		Bedlam,
		Coma,
		LegalTrickery,
		Humour,
		Potion
	}

	public enum CharacterSounds {
		MaleGreeting1,
		MaleGreeting2,
		MaleGreeting3,
		FemaleGreeting1,
		FemaleGreeting2,
		FemaleGreeting3
	}

	public enum DialogueSound {
		Test1,
		JackIntro
	}

	public enum CompanionSounds {
		Test1,
		Test2,
		OneFiftyBeats,
		Oof,
		SoYouHuman,
		CantGoOn,
		BeatBeatBeat,
		Paul,
		Mark
	}

	public enum InfoPropSound {
		SearchPaperBag,
		SearchBag,
		SearchBag2,
		RustleBin
	}

	public enum HeartSound {
		HeartSound60
	}

	public static Dictionary<InfoPropSound, string> InfoPropSoundPaths = new Dictionary<InfoPropSound, string>()
	{
		{InfoPropSound.SearchPaperBag, "res://Props/InfoProp/Sound/SearchPaperBag.wav"},
		{InfoPropSound.RustleBin, "res://Props/InfoProp/Sound/RustleBin.wav"},
		{InfoPropSound.SearchBag2, "res://Props/InfoProp/Sound/SearchBag2.wav"},
		{InfoPropSound.SearchBag, "res://Props/InfoProp/Sound/SearchBag.wav"}
	};

	public static Dictionary<MainMenuSounds, string> MainMenuSoundPaths = new Dictionary<MainMenuSounds, string>()
	{
		{MainMenuSounds.Music, "res://Stages/MainMenu/Music/Menu-instruments.ogg"},
		{MainMenuSounds.Intro, "res://Stages/MainMenu/Sound/Intro_PictureStory_v0.1.wav"},
	};

	public static Dictionary<WorldSounds, string> WorldSoundPaths = new Dictionary<WorldSounds, string>()
	{
		{WorldSounds.Music, "res://Stages/World/Music/60bpm_NoDrum.ogg"},
		{WorldSounds.Paper, "res://Stages/World/HUD/Journal/PageTurn.wav"},
		{WorldSounds.Paper2, "res://Stages/World/HUD/Journal/Paper.wav"},
		{WorldSounds.Win, "res://Stages/World/Story/Confession.wav"},
		{WorldSounds.MinigameMusic, "res://Stages/World/Music/minigamemusic.ogg"},
		{WorldSounds.Lose, "res://Stages/World/Story/dfeat.wav"}
	};


	public static Dictionary<RoomSounds, string> RoomSoundPaths = new Dictionary<RoomSounds, string>()
	{
		// {RoomSounds.Open, "res://Interface/Sounds/Click.wav"}
	};
	public static Dictionary<MiniGameSound, string> MiniGameSoundPaths = new Dictionary<MiniGameSound, string>()
	{
		{MiniGameSound.HeartBolt, "res://Props/MiniGame/Sound/HeartBolt.wav"},
		{MiniGameSound.ArcaneLaw, "res://Props/MiniGame/Sound/ArcaneLaw.wav"},
		{MiniGameSound.Bedlam, "res://Props/MiniGame/Sound/Bedlam.wav"},
		{MiniGameSound.Coma, "res://Props/MiniGame/Sound/Coma.wav"},
		{MiniGameSound.Delirium, "res://Props/MiniGame/Sound/Delirium.wav"},
		{MiniGameSound.FleshWound, "res://Props/MiniGame/Sound/FleshWound.wav"},
		{MiniGameSound.GreySoul, "res://Props/MiniGame/Sound/GreySoul.wav"},
		{MiniGameSound.MagicalBarrier, "res://Props/MiniGame/Sound/MagicalBarrier.wav"},
		{MiniGameSound.Plague, "res://Props/MiniGame/Sound/Plague.wav"},
		{MiniGameSound.Recuperate, "res://Props/MiniGame/Sound/Recuperate.wav"},
		{MiniGameSound.Sedation, "res://Props/MiniGame/Sound/Sedation.wav"},
		{MiniGameSound.Transfusion, "res://Props/MiniGame/Sound/Transfusion.wav"},
		{MiniGameSound.Humour, "res://Props/MiniGame/Sound/Humour.wav"},
		{MiniGameSound.LegalTrickery, "res://Props/MiniGame/Sound/LegalTrickery.wav"},
		{MiniGameSound.Potion, "res://Props/MiniGame/Sound/Gulp.wav"},
	};
	public static Dictionary<DoorSound, string> DoorSoundPaths = new Dictionary<DoorSound, string>()
	{
		{ DoorSound.Open, "res://Props/Door/Sound/Creak.wav"},
		{ DoorSound.Door, "res://Props/Door/Sound/Door.wav"},
		{ DoorSound.Door2, "res://Props/Door/Sound/Door2.wav"},
		{ DoorSound.Hole, "res://Props/Door/Sound/Hole.wav"},
		{ DoorSound.Hole2, "res://Props/Door/Sound/Hole2.wav"}
		// {RoomSounds.Open, "res://Interface/Sounds/Click.wav"}
	};

	public static Dictionary<NoughtsSounds, string> NoughtsSoundPaths = new Dictionary<NoughtsSounds, string>()
	{
		{ NoughtsSounds.Start, "res://Props/NoughtsCrossesGame/TicTacToeStart.wav"},
		{ NoughtsSounds.End, "res://Props/NoughtsCrossesGame/TicTacToeEnd.wav"},
		{ NoughtsSounds.CheatVictory, "res://Props/NoughtsCrossesGame/WinTicTacToev0.1.wav"},
		// {RoomSounds.Open, "res://Interface/Sounds/Click.wav"}
	};
	public static Dictionary<CompanionSounds, string> CompanionSoundPaths = new Dictionary<CompanionSounds, string>()
	{
		{ CompanionSounds.Test1, "res://Stages/World/Sounds/placeholder.wav"},
		{ CompanionSounds.Test2, "res://Stages/World/Sounds/placeholder.wav"},
		{ CompanionSounds.BeatBeatBeat, "res://Actors/Companion/Sound/companionBeatBeatBeat.wav"},
		{ CompanionSounds.CantGoOn, "res://Actors/Companion/Sound/companionIcantgo.wav"},
		{ CompanionSounds.OneFiftyBeats, "res://Actors/Companion/Sound/companion150Beats.wav"},
		{ CompanionSounds.Oof, "res://Actors/Companion/Sound/companionOof.wav"},
		{ CompanionSounds.SoYouHuman, "res://Actors/Companion/Sound/companionSoYoureTheHuman.wav"},
		{ CompanionSounds.Paul, "res://Actors/Companion/Sound/Companion_Paul.wav"},
		{ CompanionSounds.Mark, "res://Actors/Companion/Sound/Companion_mark.wav"}
		// {RoomSounds.Open, "res://Interface/Sounds/Click.wav"}
	};

	public static Dictionary<DialogueSound, string> DialogueSoundPaths = new Dictionary<DialogueSound, string>()
	{
		{ DialogueSound.Test1, "res://Stages/World/Sounds/placeholder.wav"},
		{ DialogueSound.JackIntro, "res://Props/BadHeart/HeartSoundF.wav"}
	};

	public static Dictionary<CharacterSounds, string> CharacterSoundPaths = new Dictionary<CharacterSounds, string>()
	{
		{ CharacterSounds.MaleGreeting1, "res://Actors/Character/Sounds/MaleGreeting1.wav"},
		{ CharacterSounds.MaleGreeting2, "res://Actors/Character/Sounds/MaleGreeting2.wav"},
		{ CharacterSounds.MaleGreeting3, "res://Actors/Character/Sounds/MaleGreeting3.wav"},
		{ CharacterSounds.FemaleGreeting1, "res://Actors/Character/Sounds/FemaleGreeting1.wav"},
		{ CharacterSounds.FemaleGreeting2, "res://Actors/Character/Sounds/FemaleGreeting2.wav"},
		{ CharacterSounds.FemaleGreeting3, "res://Actors/Character/Sounds/FemaleGreeting3.wav"}
	};

	public static Dictionary<ButtonSounds, string> ButtonSoundPaths = new Dictionary<ButtonSounds, string>()
	{
		{ButtonSounds.Click, "res://Interface/Buttons/BtnBase/ButtonClickScifi.wav"},
		{ButtonSounds.Hover, "res://Interface/Buttons/BtnBase/ButtonHoverSciFi.wav"},
	};

	public static Dictionary<HeartSound, string> HeartSoundPaths = new Dictionary<HeartSound, string>()
	{
		{HeartSound.HeartSound60, "res://Props/BadHeart/HeartSoundF.wav"}
	};
	// Return a dictionary containing loaded sounds. Useful to do when loading/instancing a scene e.g. player.
	// Usage: Dictionary<AudioData.PlayerSounds, AudioStream> playerSounds = 
	//  AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);
	// Change to AudioStreamPlayer for music/global sounds and AudioStreamPlayer3D if working in 3D
	public static Dictionary<T, AudioStream> LoadedSounds<T>(Dictionary<T, string> soundPathsDict)
	{
		Dictionary<T, AudioStream> loadedSoundsDict = new Dictionary<T, AudioStream>();
		foreach (T key in soundPathsDict.Keys)
		{
			loadedSoundsDict[key] = (AudioStream) GD.Load(soundPathsDict[key]);
		}
		return loadedSoundsDict;
	}


}
