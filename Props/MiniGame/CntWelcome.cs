using Godot;
using System;

public class CntWelcome : Control
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private Button _btnStart;
	private Button _btnReady;
	private Button _btnSkillUp;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_btnStart = GetNode<Button>("BtnStart");
		_btnReady = GetNode<Button>("BtnReady");
		_btnSkillUp = GetNode<Button>("BtnSkillUp");
	}

	public void UpdateText(string text)
	{
		GetNode<Label>("LblIntro").Text = text;
	}

	public void HideAllButtons()
	{
		_btnStart.Visible = false;
		_btnReady.Visible = false;
		_btnSkillUp.Visible = false;
	}

}
