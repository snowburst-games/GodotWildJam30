using Godot;
using System;

public class DurationEffect
{	
	public string EffectName {get; set;}
	public int DisableDuration {get; set;} = 0;
	public int DamageDuration {get; set;} = 0;
	public int Damage {get; set;} = 0;
	public int ConfuseDuration {get; set;} = 0;
	public bool TargetSelf {get; set;} = false;
	
	public int BuffResistanceDuration {get; set;} = 0;
	public int BuffSolemnityDuration {get; set;} = 0;
	public int BuffIndemnityDuration {get; set;} = 0;

}
