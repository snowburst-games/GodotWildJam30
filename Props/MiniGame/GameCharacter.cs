using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class GameCharacter : Reference
{
	[Signal]
	public delegate void PlayerSpendingSkillPoints();

	public enum CharacterAttribute {Sorcery, Resistance, Humour, Solemnity, LegalTrickery, Indemnity, Initiative, Luck}
	public enum CharacterStat {HitPoints, Mana, TotalHitPoints, TotalMana, Experience, Level}
	
	public Dictionary<CharacterAttribute, int> Attributes {get; set;} = new Dictionary<CharacterAttribute, int>() {
		{CharacterAttribute.Sorcery, 1},
		{CharacterAttribute.Resistance, 1},
		{CharacterAttribute.Humour, 1},
		{CharacterAttribute.Solemnity, 1},
		{CharacterAttribute.LegalTrickery, 1},
		{CharacterAttribute.Indemnity, 1},
		{CharacterAttribute.Initiative, 1},
		{CharacterAttribute.Luck, 1}
	};
	public Dictionary<CharacterStat, int> Stats {get; set;} = new Dictionary<CharacterStat, int>() {
		{CharacterStat.HitPoints, 10},
		{CharacterStat.TotalHitPoints, 10},
		{CharacterStat.Mana, 5},
		{CharacterStat.TotalMana, 5},
		{CharacterStat.Experience, 0},
		{CharacterStat.Level, 1}
	};


	public bool Disabled {get; set;} = false;
	public bool Confused {get; set;} = false;
	public bool SolemnityBuff {get; set;} = false;
	public bool IndemnityBuff {get; set;} = false;
	public bool ResistanceBuff {get; set;} = false;

	public bool Player {get; set;} = false;

	public int SkillPoints {get; set;}

	public int HealthPotions {get; set;} = 0;
	public int ManaPotions {get; set;} = 0;

	public int UID {get; set;}
	public string CharacterName {get; set;}

	public List<Spell> KnownSpells = new List<Spell>();

	public List<DurationEffect> CurrentEffects = new List<DurationEffect>();

	private Random _rand = new Random();

	public DialogueControl.CharacterEnum DialogueCharacter;

	public GameCharacter()
	{
		GD.Print("Invalid constructor called.");
		throw new InvalidOperationException();
	}
	public GameCharacter(int uid, string name, int level, bool player, DialogueControl.CharacterEnum dialogueCharacter)
	{
		UID = uid;
		CharacterName = name;
		Stats[GameCharacter.CharacterStat.Level] = level;
		Player = player;
		DialogueCharacter = dialogueCharacter;

		for (int i = 0; i < level; i++)
		{
			SkillPoints += i + 3;
		}
		SetStatsByLevel();
	}

	public void SpendSkillPoints()
	{
		if (Player)
		{
			PlayerSpendSkillPoints();
		}
		else
		{
			RandomSpendSkillpoints();
			// Attributes[GameCharacter.CharacterAttribute.Initiative] += 10;
		}
	}

	public void LevelUp()
	{
		Stats[GameCharacter.CharacterStat.Level] += 1;
		SkillPoints += Stats[GameCharacter.CharacterStat.Level];
		SetStatsByLevel();
	}

	public void SetStatsByLevel()
	{
		Stats[GameCharacter.CharacterStat.TotalHitPoints] = Convert.ToInt32(Math.Pow(7*(Stats[GameCharacter.CharacterStat.Level]),1.2));
		Stats[GameCharacter.CharacterStat.TotalMana] = Convert.ToInt32(Math.Pow(4*(Stats[GameCharacter.CharacterStat.Level]),1.2));

		if (Player)
		{
			Stats[GameCharacter.CharacterStat.TotalMana] = Convert.ToInt32(Stats[GameCharacter.CharacterStat.TotalMana] * 1.2);
			Stats[GameCharacter.CharacterStat.TotalHitPoints] = Convert.ToInt32(Stats[GameCharacter.CharacterStat.TotalHitPoints] * 1.2);
		}
		Stats[GameCharacter.CharacterStat.HitPoints] = Stats[GameCharacter.CharacterStat.TotalHitPoints];
		Stats[GameCharacter.CharacterStat.Mana] = Stats[GameCharacter.CharacterStat.TotalMana];
	}

	public void LearnRandomSpells(List<Spell> spellPool)
	{
		while (KnownSpells.Count < 4)
		{
			Spell spell = spellPool[_rand.Next(spellPool.Count)];
			if (!KnownSpells.Contains(spell))
			{
				KnownSpells.Add(spell);
			}
		}
		// Attributes[CharacterAttribute.Sorcery] += 200;
		// Stats[CharacterStat.TotalMana] += 200;
		// Stats[CharacterStat.Mana] += 200;

		// KnownSpells.Add(spellPool.Find(x => x.SpellName == "Transfusion"));
		// KnownSpells.Add(spellPool.Find(x => x.SpellName == "Recuperate"));
		// KnownSpells.Add(spellPool.Find(x => x.SpellName == "Magical Barrier"));
		// KnownSpells.Add(spellPool.Find(x => x.SpellName == "Arcane Law"));
		// GD.Print("HERE: " + spellPool.Find(x => x.SpellName == "Transfusion"));
		// GD.Print("HERE:");
		// foreach (Spell spell in spellPool)
		// {
		// 	GD.Print(spell.SpellName);
		// }
	}



	public void LearnSpell(Spell spell)
	{
		if (!KnownSpells.Contains(spell))
		{
			KnownSpells.Add(spell);
		}
	}

	
	// https://www.desmos.com/calculator
	public int GetExperienceNeededForLevel(int level)
	{
		return Convert.ToInt32(Math.Floor((5* Math.Pow(level, 3)) / 8));
	}

	public int GetXPFromThisLevelToNextLevel()
	{
		int xpForNextLevel = GetExperienceNeededForLevel(Stats[GameCharacter.CharacterStat.Level]+1);
		int xpForThisLevel = GetExperienceNeededForLevel(Stats[GameCharacter.CharacterStat.Level]);
		return xpForNextLevel - xpForThisLevel;
	}

	public int GetXPSinceThisLevel()
	{
		int xpForThisLevel = GetExperienceNeededForLevel(Stats[GameCharacter.CharacterStat.Level]);
		return Stats[GameCharacter.CharacterStat.Experience] - xpForThisLevel;
	}

	public void PlayerSpendSkillPoints()
	{
		EmitSignal(nameof(PlayerSpendingSkillPoints));
	}

	public int GetXPValue()
	{
		return Convert.ToInt32((Math.Pow(2*Stats[GameCharacter.CharacterStat.Level], 2) +9)/2.5);
	}

	public void UpdateExperience(int exp)
	{
		Stats[GameCharacter.CharacterStat.Experience] += exp + 
			(_rand.Next(0,Attributes[GameCharacter.CharacterAttribute.Luck]) != 0 ? _rand.Next(0,Attributes[GameCharacter.CharacterAttribute.Luck]) : 0);

		if (Stats[GameCharacter.CharacterStat.Experience] >= GetExperienceNeededForLevel(Stats[GameCharacter.CharacterStat.Level]+1))
		{
			// GD.Print("Can level");
			LevelUp();
			UpdateExperience(0); // keep running until we can no longer level (levelling automatically based on our xp)
		}
	}

	public void RandomSpendSkillpoints()
	{
		while (SkillPoints > 0)
		{
			foreach (CharacterAttribute attribute in Attributes.Keys.ToList())
			{
				int skillPointsToSpend = _rand.Next(0,Convert.ToInt32(Math.Ceiling((double)SkillPoints/2))+1);
				Attributes[attribute] += skillPointsToSpend;
				SkillPoints -= skillPointsToSpend;
			}
		}

		GD.Print("Skill points for " + CharacterName + ":");
		foreach (CharacterAttribute attribute in Attributes.Keys.ToList())
		{
			GD.Print(attribute.ToString() + ": " + Attributes[attribute]);
		}
	}

	public void SetPotionsByLevel()
	{
		HealthPotions = Math.Min(_rand.Next(0, Convert.ToInt32(Math.Ceiling((double)Stats[GameCharacter.CharacterStat.Level]/3))+1), 2);
		ManaPotions = Math.Min(_rand.Next(0, Convert.ToInt32(Math.Ceiling((double)Stats[GameCharacter.CharacterStat.Level]/3))+1), 2);
	}

	public int CalculateLawsuitInitialDamage()
	{
		return Convert.ToInt32(Attributes[CharacterAttribute.LegalTrickery] * Math.Pow(1+(Attributes[CharacterAttribute.LegalTrickery]/10),1.5))
		+ GetLuckyNumber();
	}
	
	public int GetLuckyNumber()
	{
		return _rand.Next(0,Attributes[GameCharacter.CharacterAttribute.Luck]+1) != 0 ? _rand.Next(0,Attributes[GameCharacter.CharacterAttribute.Luck]+1)/4 : 0;
	}

	public int DefendAgainstLawsuitDamage(int incomingDamage)
	{
		OnDamageIsSleepRemoved();
		int calculatedDamage = Convert.ToInt32(incomingDamage/Math.Pow(1+(Attributes[CharacterAttribute.Indemnity]/20),2));
		calculatedDamage = IndemnityBuff ? Convert.ToInt32(calculatedDamage * 0.66) : calculatedDamage;
		ModHitPoints(-Math.Max(1,calculatedDamage));
		return calculatedDamage; // calculated damage
	}
	
	public int CalculateJokeInitialDamage()
	{
		return Convert.ToInt32(Attributes[CharacterAttribute.Humour] * Math.Pow(1+(Attributes[CharacterAttribute.Humour]/10),1.5))
		+ GetLuckyNumber();
	}

	public int DefendAgainstJokeDamage(int incomingDamage)
	{
		OnDamageIsSleepRemoved();
		int calculatedDamage = Convert.ToInt32(incomingDamage/Math.Pow(1+(Attributes[CharacterAttribute.Solemnity]/20),2));
		calculatedDamage = SolemnityBuff ? Convert.ToInt32(calculatedDamage * 0.66) : calculatedDamage;
		ModHitPoints(-Math.Max(1,calculatedDamage));
		return calculatedDamage; // calculated damage
	}
	public void ClearConfusionEffects()
	{
		foreach (DurationEffect effect in CurrentEffects.ToList())
		{
			if (effect.ConfuseDuration > 0)
			{
				CurrentEffects.Remove(effect);
			}
		}
		Confused = false;
	}
	public void ClearSleepEffects()
	{
		foreach (DurationEffect effect in CurrentEffects.ToList())
		{
			if (effect.DisableDuration > 0)
			{
				CurrentEffects.Remove(effect);
			}
		}
		Disabled = false;
	}

	public bool CannotAffordAnySpell()
	{
		foreach (Spell spell in KnownSpells)
		{
			if (Stats[GameCharacter.CharacterStat.Mana] >= spell.ManaCost)
			{
				return false;
			}
		}
		return true;
	}

	public bool CanAffordSpell(Spell spell)
	{
		GD.Print("Character mana is: " + Stats[GameCharacter.CharacterStat.Mana]);
		GD.Print("Spell mana is: " + spell.ManaCost);
		if (Stats[GameCharacter.CharacterStat.Mana] >= spell.ManaCost)
		{
			return true;
		}
		return false;
	}

	public string GetRandomAppropriateSpellThatCanAfford()
	{
		
		List<Spell> canAffordSpells = new List<Spell>();
		foreach (Spell spell in KnownSpells.OrderBy(x => Guid.NewGuid()).ToList())
		{
			if (CanAffordSpell(spell))
			{
				canAffordSpells.Add(spell);
			}
		}

		string spellToCast = "";

		foreach (Spell spell in canAffordSpells)
		{
			if (spell.TargetSelf && spell.Damage < 0 && Stats[CharacterStat.HitPoints] >= Stats[CharacterStat.TotalHitPoints])
			{
				continue;
			}
			if (spell.BuffIndemnityDuration > 0 && IndemnityBuff)
			{
				continue;
			}
			if (spell.BuffSolemnityDuration > 0 && SolemnityBuff)
			{
				continue;
			}
			if (spell.BuffResistanceDuration > 0 && ResistanceBuff)
			{
				continue;
			}
			spellToCast = spell.SpellName;
		}

		// Spell randomSpell = canAffordSpells[_rand.Next(0,canAffordSpells.Count)];

		// if (randomSpell.TargetSelf == true && randomSpell.Damage < 0) 
		// // we need to rework this method otherwise it will crash the game
		// {
		// 	return GetRandomAppropriateSpellThatCanAfford();
		// }
		// // if has relevant buff already, abort
		// else if (randomSpell.BuffIndemnityDuration > 0 && IndemnityBuff)
		// {
		// 	return GetRandomAppropriateSpellThatCanAfford();
		// }
		// else if (randomSpell.BuffSolemnityDuration > 0 && SolemnityBuff)
		// {
		// 	return GetRandomAppropriateSpellThatCanAfford();
		// }
		// else if (randomSpell.BuffResistanceDuration > 0 && ResistanceBuff)
		// {
		// 	return GetRandomAppropriateSpellThatCanAfford();
		// }
		// else
		// {
		// 	return randomSpell.SpellName;
		// }
		return spellToCast;
	}

	public bool IsAfflictedWithDot()
	{
		foreach (DurationEffect currentEffect in CurrentEffects)
		{
			if (currentEffect.DamageDuration > 0 && !currentEffect.TargetSelf)
			{
				return true;
			}
		}
		return false;
	}


	public void AfflictWithJokeSleepEffect()
	{
		DurationEffect effect = new DurationEffect() {
				DisableDuration = 2,
				TargetSelf = false,
				EffectName = "Boring Joke"
			};
		foreach (DurationEffect currentEffect in CurrentEffects.ToList())
		{
			if (currentEffect.EffectName == effect.EffectName)
			{
				CurrentEffects.Remove(currentEffect);
			}
		}
		CurrentEffects.Add(effect);
		Disabled = true;
		// DoDurationEffect(effect);
	}
	public void AfflictWithLawsuitConfuseEffect()
	{
		DurationEffect effect = new DurationEffect() {
				ConfuseDuration = 2,
				TargetSelf = false,
				EffectName = "Legal Befuddlement"
			};
		foreach (DurationEffect currentEffect in CurrentEffects.ToList())
		{
			if (currentEffect.EffectName == effect.EffectName)
			{
				CurrentEffects.Remove(currentEffect);
			}
		}
		CurrentEffects.Add(effect);
		Confused = true;
		// DoDurationEffect(effect);
	}
	
	public bool IsAfflictedWithSleep()
	{

		// GD.Print("HERE IT SOULD BE manu !!!!!!!!!!");
		// GD.Print(CurrentEffects.Count);
		foreach (DurationEffect currentEffect in CurrentEffects)
		{
			if (currentEffect.DisableDuration > 0)
			{
				// GD.Print("HERE IT SOULD BE POSITIVE !!!!!!!!!!");
				// GD.Print(currentEffect.EffectName);
				return true;
			}
		}
		return false;
	}

	public bool IsAfflictedWithHot()
	{
		foreach (DurationEffect currentEffect in CurrentEffects)
		{
			if (currentEffect.DamageDuration > 0 && currentEffect.TargetSelf)
			{
				return true;
			}
		}
		return false;
	}

	public int TakeSpellEffect(Spell spell, int sorcery, int luckyMagnitude)
	{
		if (spell.DisableDuration > 0 || spell.DamageDuration > 0 || spell.ConfuseDuration > 0 || spell.BuffIndemnityDuration > 0
			 || spell.BuffResistanceDuration > 0 || spell.BuffSolemnityDuration > 0)
		{
			DurationEffect effect = new DurationEffect() {
				DisableDuration = Convert.ToInt32((spell.DisableDuration * Math.Pow(1+(sorcery/10),2))/Math.Pow(1+(Attributes[CharacterAttribute.Resistance]/10),2)),
				DamageDuration = Convert.ToInt32((spell.DamageDuration * Math.Pow(1+(sorcery/10),2))/Math.Pow(1+(Attributes[CharacterAttribute.Resistance]/10),2)),
				ConfuseDuration = Convert.ToInt32((spell.ConfuseDuration * Math.Pow(1+(sorcery/10),2))/Math.Pow(1+(Attributes[CharacterAttribute.Resistance]/10),2)),
				BuffIndemnityDuration = Convert.ToInt32(spell.BuffIndemnityDuration * Math.Pow(1+(sorcery/10),2)),
				BuffResistanceDuration = Convert.ToInt32(spell.BuffResistanceDuration * Math.Pow(1+(sorcery/10),2)),
				BuffSolemnityDuration = Convert.ToInt32(spell.BuffSolemnityDuration * Math.Pow(1+(sorcery/10),2)),
				Damage = Convert.ToInt32(spell.Damage * Math.Pow(1+(sorcery/6),1.5)),
				TargetSelf = spell.TargetSelf,
				EffectName = spell.SpellName
			};
			foreach (DurationEffect currentEffect in CurrentEffects.ToList())
			{
				if (currentEffect.EffectName == effect.EffectName)
				{
					CurrentEffects.Remove(currentEffect);
				}
			}
			CurrentEffects.Add(effect);
			DoDurationEffect(effect);

		}
		if (spell.TargetSelf)
		{
			// GD.Print("HEAL");
			// GD.Print(spell.Damage);
			// GD.Print(luckyMagnitude);
			int incomingHeal = Convert.ToInt32(spell.Damage * Math.Pow(1+(sorcery/10),2) - luckyMagnitude);
			// GD.Print(incomingHeal);

			Heal(incomingHeal);
			return incomingHeal;
		}
		else
		{
			bool wakeUp = true;
			if (spell.DisableDuration > 0 || spell.ConfuseDuration > 0)
			{
				luckyMagnitude = 0;
			}
			if (spell.Damage == 0 || spell.DisableDuration > 0)
			{
				wakeUp = false;
			}
			return DefendAgainstSpellDamage(spell.Damage + luckyMagnitude, wakeUp);	
		}
	}

	public void ResetStatusEffects()
	{
		Confused = Disabled = SolemnityBuff = IndemnityBuff = ResistanceBuff = false;
	}

	public void DoDurationEffect(DurationEffect effect)
	{
		
		if (effect.DamageDuration > 0)
		{
			if (effect.TargetSelf)
			{
				Heal(effect.Damage);
			}
			else
			{
				DefendAgainstSpellDamage(effect.Damage, false);
			}
			effect.DamageDuration -= 1;
		}
		if (effect.ConfuseDuration > 0)
		{
			Confused = true;
			effect.ConfuseDuration -= 1;
		}
		if (effect.DisableDuration > 0)
		{
			Disabled = true;
			effect.DisableDuration -= 1;
		}
		if (effect.BuffSolemnityDuration > 0)
		{
			SolemnityBuff = true;
			effect.BuffSolemnityDuration -= 1;
		}
		if (effect.BuffIndemnityDuration > 0)
		{
			IndemnityBuff = true;
			effect.BuffIndemnityDuration -= 1;
		}
		if (effect.BuffResistanceDuration > 0)
		{
			ResistanceBuff = true;
			effect.BuffResistanceDuration -= 1;
		}

		if (effect.DamageDuration == 0 && effect.ConfuseDuration == 0 && effect.DisableDuration == 0 && effect.BuffSolemnityDuration == 0
			 && effect.BuffIndemnityDuration == 0 && effect.BuffResistanceDuration == 0)
		{
			CurrentEffects.Remove(effect);
		}
	}

	public bool OnDamageIsSleepRemoved()
	{
		if (Disabled)
		{
			Disabled = false;
			foreach (DurationEffect effect in CurrentEffects.ToList())
			{
				if (effect.DisableDuration > 0)
				{
					CurrentEffects.Remove(effect);
				}
			}
			return true;
		}

		return false;
	}

	public bool[] GetActiveAfflictions() // damage = 0, confuse = 1, disable = 2
	{
		bool[] activeDurationEffects = new bool[3] {false,false,false};
		foreach (DurationEffect effect in CurrentEffects)
		{
			activeDurationEffects[0] = effect.DamageDuration > 0;
			activeDurationEffects[1] = effect.ConfuseDuration > 0;
			activeDurationEffects[2] = effect.DisableDuration > 0;
		}
		return activeDurationEffects;
	}

	// public void DoAllDurationEffects()
	// {
	// 	foreach(DurationEffect effect in _currentEffects.ToList())
	// 	{
	// 		DoDurationEffect(effect);
	// 	}
	// }

	public void Heal(int incomingDamage)
	{
		ModHitPoints(-incomingDamage);
	}

	public int DefendAgainstSpellDamage(int incomingDamage, bool wakeUp=true)
	{
		if (wakeUp)
		{
			OnDamageIsSleepRemoved();
		}
		int calculatedDamage = Convert.ToInt32(incomingDamage/Math.Pow(1+(Attributes[CharacterAttribute.Resistance]/10),2));
		calculatedDamage = ResistanceBuff ? Convert.ToInt32(calculatedDamage * 0.66) : calculatedDamage;
		ModHitPoints(-Math.Max(1,calculatedDamage));
		return calculatedDamage;
	}

	public int DefendAgainstPureDamage(int incomingDamage)
	{
		OnDamageIsSleepRemoved();
		ModHitPoints(-incomingDamage);
		return incomingDamage;
	}

	public void ModHitPoints(int amount)
	{
		Stats[CharacterStat.HitPoints] = Mathf.Clamp(Stats[CharacterStat.HitPoints] + amount, 0, Stats[CharacterStat.TotalHitPoints]);
		// GD.Print("hit points are now ", Stats[CharacterStat.HitPoints]);
	}
	public void ModMana(int amount)
	{
		Stats[CharacterStat.Mana] = Mathf.Clamp(Stats[CharacterStat.Mana] + amount, 0, Stats[CharacterStat.TotalMana]);
		// GD.Print("hit points are now ", Stats[CharacterStat.HitPoints]);
	}

	public void ResetCharacter()
	{
		Stats[CharacterStat.HitPoints] = Stats[CharacterStat.TotalHitPoints];
		Stats[CharacterStat.Mana] = Stats[CharacterStat.TotalMana];
		Confused = false;
		Disabled = false;
		IndemnityBuff = false;
		ResistanceBuff = false;
		SolemnityBuff = false;
		CurrentEffects.Clear();
	}

}
