using Godot;
using System;
using System.Collections.Generic;

public class CntLearnSpells : Control
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private PackedScene _scnBtnBase;

	public List<Button> SpellButtons {get; set;} = new List<Button>();
	private GridContainer _gridContainer;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_scnBtnBase = GD.Load<PackedScene>("res://Interface/Buttons/BtnBase/BtnBase.tscn");
		_gridContainer = GetNode<GridContainer>("Grid");
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }

	public void PopulateGrid(List<Spell> spells, List<Spell> knownSpells)
	{
		foreach (Button btn in SpellButtons)
		{
			btn.QueueFree();
		}
		SpellButtons.Clear();
		foreach (Spell spell in spells)
		{
			AddButton(spell);
			if (knownSpells.Contains(spell))
			{
				OnSpellLearned(spell);
			}
		}
			// SpellButtons[i].SetMeta("Spell", spells[i].SpellName);
	}

	private void AddButton(Spell spell)
	{
		// foreach (Button btn in SpellButtons)
		// {
		// 	if (btn.Text == spell.SpellName)
		// 	{
		// 		return;
		// 	}
		// }
		BtnBase btnBase = (BtnBase) _scnBtnBase.Instance();
		btnBase.SetMeta("Spell", spell.SpellName);
		btnBase.Text = spell.SpellName;
		btnBase.HintTooltip = spell.Tooltip;
		btnBase.SizeFlagsHorizontal = (int) SizeFlags.ExpandFill;
		btnBase.SizeFlagsVertical = (int) SizeFlags.ExpandFill;
		_gridContainer.AddChild(btnBase);
		SpellButtons.Add(btnBase);
	}

	public void OnSpellLearned(Spell spell)
	{
		Button btn = SpellButtons.Find(x => (string)x.GetMeta("Spell") == spell.SpellName);
		btn.Disabled = true;
	}

}
