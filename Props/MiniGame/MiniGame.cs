using Godot;
using System;

public class MiniGame : Panel
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	[Signal]
	public delegate void MiniGameClosed();

	public PnlPlayArea PlayArea;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		PlayArea = GetNode<PnlPlayArea>("PnlPlayArea");
		GetNode<Button>("PnlHUD/HBox/BtnReset").Disabled = true;
		GetNode<Panel>("PnlConfirmReset").Visible = false;
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
	private void OnBtnClosePressed()
	{
		// Visible = false;
		EmitSignal(nameof(MiniGame.MiniGameClosed));
	}

	private void OnBtnStartPressed()
	{
		GetNode<Button>("PnlHUD/HBox/BtnReset").Disabled = false;
	}

	private void OnBtnResetPressed()
	{
		GetNode<Panel>("PnlConfirmReset").Visible = true;
	}

	private void OnResetBtnConfirmPressed()
	{
		PlayArea.Start();
		GetNode<Panel>("PnlConfirmReset").Visible = false;
	}


	private void OnResetBtnCancelPressed()
	{
		GetNode<Panel>("PnlConfirmReset").Visible = false;
	}

}



