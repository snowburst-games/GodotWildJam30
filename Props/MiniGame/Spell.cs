using Godot;
using System;

public class Spell
{
	public int DisableDuration {get; set;} = 0;
	public int DamageDuration {get; set;} = 0;
	public int Damage {get; set;} = 0;
	public int ConfuseDuration {get; set;} = 0;
	public int ManaCost {get; set;} = 0;
	public string SpellName {get; set;} = "";
	public bool TargetSelf {get; set;} = false;
	public string Tooltip {get; set;} = "";

	public int BuffResistanceDuration {get; set;} = 0;
	public int BuffSolemnityDuration {get; set;} = 0;
	public int BuffIndemnityDuration {get; set;} = 0;

}
