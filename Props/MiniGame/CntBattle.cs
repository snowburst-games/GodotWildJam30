
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class CntBattle : Control
{
	private GameCharacterDisplay _playerDisplay;
	private GameCharacterDisplay _opponentDisplay;
	private AudioStreamPlayer _soundPlayer;
	private Dictionary<AudioData.MiniGameSound, AudioStream> _miniGameSounds = AudioData.LoadedSounds<AudioData.MiniGameSound>(AudioData.MiniGameSoundPaths);

	private Control _cntControls;
	private Control _cntAction;
	private CntAbilities  _cntAbilities;
	private CntPotions  _cntPotions;
	private Label _lblAction;

	public List<Spell> SpellPool {get; set;}

	private AnimationPlayer _anim;

	private Timer _actionTimer;
	private AnimationPlayer _actionTextAnim;

	private TurnManager _turnManager;
	private Random _rand = new Random();

	[Signal]
	public delegate void PlayerWin();

	[Signal]
	public delegate void PlayerLost();

	private List<string> _jokes = new List<string>()
	{
		"Did you hear about the optometrist that tripped and fell into the hole in the hospital floor? He made a spectacle of himself!",
		"What do you call an offal kebab? An organ doner!",
		"When does an apple a day keep the doctor away? When you throw it hard enough!",
		"Which department is terrible at hide and seek? ICU!",
		"What do you call a doctor who makes rash decisions? A dermatologist!",
		"Why did the computer scientist keep killing patients? They kept trying to turn them off and on again!",
		// "A patient says to his doctor, \"Doctor, My rear end has been in pain since you gave me these new painkillers!\". The doctor says, \"Are you taking them as directed?\" And the patient replied, 'Yes!,' The packet says, 'pierce foil and push up bottom'.",
		"What did the man say when his pet snake beat him in a 100 meter race? Me spine held me back!",
		"Why was the artichoke embarrassed? Because it saw the chick pea!",
		"Why was the tin man looking for an artichoke? Because he needed a heart!",
		"Why did Marky the receptionist reject the part-time cardiologist every time she asked him out on a date? Her requests were half-hearted!",
		"Why did the opera singer have to have a cardiac transplant during a performance? She sang her heart out!",
		"What did the police say to the heart thief? You're under cardiac arrest!"
	};

	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_cntControls = GetNode<Control>("PnlControlArea/CntControls");
		_cntAction = GetNode<Control>("PnlControlArea/CntAction");
		_cntAbilities = GetNode<CntAbilities>("PnlControlArea/CntAbilities");
		_cntPotions = GetNode<CntPotions>("PnlControlArea/CntPotions");
		_cntControls.Visible = _cntAbilities.Visible = _cntAction.Visible = _cntPotions.Visible = false;
		_anim = GetNode<AnimationPlayer>("Anim");
		_actionTextAnim = GetNode<AnimationPlayer>("PnlControlArea/CntAction/Anim");
		_actionTimer = GetNode<Timer>("PnlControlArea/CntAction/Timer");
		_lblAction = GetNode<Label>("PnlControlArea/CntAction/LblInfo");
		_playerDisplay = GetNode<GameCharacterDisplay>("Player");	
		_opponentDisplay = GetNode<GameCharacterDisplay>("Opponent");
		_playerDisplay.Connect(nameof(GameCharacterDisplay.StartingTurn), this, nameof(OnPlayerStartingTurn));
		_opponentDisplay.Connect(nameof(GameCharacterDisplay.StartingTurn), this, nameof(OnOpponentStartingTurn));
		_playerDisplay.Connect(nameof(GameCharacterDisplay.EndingTurn), this, nameof(OnPlayerEndingTurn));
		_opponentDisplay.Connect(nameof(GameCharacterDisplay.EndingTurn), this, nameof(OnOpponentEndingTurn));

		
	}

	private void ShowOneControlAreaContainer(Control controlAreaContainer)
	{
		_cntControls.Visible = _cntAbilities.Visible = _cntAction.Visible = _cntPotions.Visible = false;
		controlAreaContainer.Visible = true;
	}

	public void SetCharacterDisplay(GameCharacter gameCharacter)
	{
		GameCharacterDisplay currentDisplay = gameCharacter.Player ? _playerDisplay : _opponentDisplay;
		currentDisplay.Initialise(gameCharacter);
		currentDisplay.SetSprite();
		currentDisplay.UpdateDisplay();
		if (!gameCharacter.Player)
		{
			currentDisplay.UpdateBestWorstIcons();
			return;
		}
		_cntAbilities.UpdateSpellButtons(gameCharacter.KnownSpells);
		foreach (Button btn in _cntAbilities.SpellButtons)
		{
			if (btn.IsConnected("pressed", this, nameof(OnSpellPressed)))
			{
				btn.Disconnect("pressed", this, nameof(OnSpellPressed));
			}
			if (!btn.Visible)
			{
				continue;
			}
			btn.Connect("pressed", this, nameof(OnSpellPressed), new Godot.Collections.Array{(string)btn.GetMeta("Spell")});
		}
	}

	public void StartBattle()
	{
		// GD.Print("sorting..");
		_turnManager = new TurnManager(new List<ITurnBasedUnit>() {_playerDisplay, _opponentDisplay});
		_turnManager.SortUnitList();
		NextTurn(); // go to the next turn
	}

	private async void OnBtnLawsuitPressed()
	{
		// Replace with function body.
		// if (!IsCurrentCharacterPlayer())
		// {
		// 	return;
		// }
		bool confuseEffect = false;
		GetCurrentGameCharacterDisplay().OnDoAction();

		_anim.Play("Lawsuit");
		PlaySoundEffect(AudioData.MiniGameSound.LegalTrickery);
		if (GetCurrentOpponentGameCharacter().IsAfflictedWithSleep())
		{
			ShowAction(String.Format("{0} wakes up!", GetCurrentOpponentGameCharacter().CharacterName),2.5f);
			await ToSignal(_actionTimer, "timeout");
		}

		int incomingDamage = GetCurrentGameCharacter().CalculateLawsuitInitialDamage();
		GD.Print("DAMAGE " + incomingDamage);
		int defendingDamage = GetCurrentOpponentGameCharacter().DefendAgainstLawsuitDamage(incomingDamage);
		TakeDamageAnim(GetOpposingGameCharacterDisplay());

		if (_rand.Next(0,8) == 0)
		{
			GetCurrentOpponentGameCharacter().AfflictWithLawsuitConfuseEffect();// Disabled = sleepEffect = true;
			confuseEffect = true;
			// GetCurrentOpponentGameCharacter().Confused = confuseEffect = true;
		}

		ShowActionEndTurn(String.Format("{0} throws the full weight of their legal team upon {1}! {2} damage! {3}",
			GetCurrentGameCharacter().CharacterName, 
			GetCurrentOpponentGameCharacter().CharacterName,
			defendingDamage, confuseEffect ? String.Format("{0} is confused by this legal trickery!", GetCurrentOpponentGameCharacter().CharacterName) : ""), confuseEffect ? 4 : 4f);

		_playerDisplay.UpdateDisplay();		
		_opponentDisplay.UpdateDisplay();		
	}

	public GameCharacterDisplay GetCurrentGameCharacterDisplay()
	{
		return GetCurrentGameCharacter().Player ? _playerDisplay : _opponentDisplay;
	}
	public GameCharacterDisplay GetOpposingGameCharacterDisplay()
	{
		return GetCurrentGameCharacter().Player ? _opponentDisplay : _playerDisplay;
	}

	// public void SetStatusEffect(GameCharacter gameCharacter, StatusPanel.StatusEffect statusEffect, bool enabled)
	// {
	// 	switch (statusEffect)
	// 	{
	// 		case StatusPanel.StatusEffect.Confused:
	// 			gameCharacter.Confused = enabled;
	// 			break;
	// 		case StatusPanel.StatusEffect.D:
	// 			gameCharacter.Confused = enabled;
	// 			break;
	// 		case StatusPanel.StatusEffect.Confused:
	// 			gameCharacter.Confused = enabled;
	// 			break;
	// 		case StatusPanel.StatusEffect.Confused:
	// 			gameCharacter.Confused = enabled;
	// 			break;
	// 		case StatusPanel.StatusEffect.Confused:
	// 			gameCharacter.Confused = enabled;
	// 			break;
	// 		case StatusPanel.StatusEffect.Confused:
	// 			gameCharacter.Confused = enabled;
	// 			break;
	// 	}
	// }

	private async void OnBtnTellJokePressed()
	{
		bool sleepEffect = false;
		GetCurrentGameCharacterDisplay().OnDoAction();

		_anim.Play("Joke");
		PlaySoundEffect(AudioData.MiniGameSound.Humour);
		if (GetCurrentOpponentGameCharacter().IsAfflictedWithSleep())
		{
			ShowAction(String.Format("{0} wakes up!", GetCurrentOpponentGameCharacter().CharacterName),2.5f);
			await ToSignal(_actionTimer, "timeout");
		}

		int incomingDamage = GetCurrentGameCharacter().CalculateJokeInitialDamage();
		int defendingDamage = 0;

		if (_rand.Next(0,6) == 0)
		{
			GetCurrentOpponentGameCharacter().AfflictWithJokeSleepEffect();// Disabled = sleepEffect = true;
			sleepEffect = true;
		}
		else
		{
			defendingDamage = GetCurrentOpponentGameCharacter().DefendAgainstJokeDamage(incomingDamage);
			TakeDamageAnim(GetOpposingGameCharacterDisplay());
		}

		ShowActionEndTurn(String.Format("{0} says: {1}\n{2}",
			GetCurrentGameCharacter().CharacterName, 
			_jokes[_rand.Next(0, _jokes.Count)],
			sleepEffect ? 
				String.Format("Terrible! {0} has fallen asleep!", GetCurrentOpponentGameCharacter().CharacterName) :
				String.Format("Hilarious! {0} takes {1} damage!", GetCurrentOpponentGameCharacter().CharacterName, defendingDamage)), 6f);

		_playerDisplay.UpdateDisplay();		
		_opponentDisplay.UpdateDisplay();
	}

	private async void TakeDamageAnim(GameCharacterDisplay gameCharacter)
	{
		await ToSignal(_anim, "animation_finished");
		gameCharacter.OnTakeDamage();
	}

	private async void PlaySoundEffect(AudioData.MiniGameSound sound)
	{
		GetNode<Timer>("SoundTimer").Start();
		await ToSignal(GetNode<Timer>("SoundTimer"), "timeout");
		AudioHandler.PlaySound(_soundPlayer, _miniGameSounds[sound], AudioData.SoundBus.Effects);
	}


	private void OnBtnPotionPressed()
	{
		_cntPotions.UpdatePotionButtons(_playerDisplay.CurrentCharacter.HealthPotions, _playerDisplay.CurrentCharacter.ManaPotions);
		ShowOneControlAreaContainer(_cntPotions);

	}

	private void OnHealthPotionPressed()
	{
		if (GetCurrentGameCharacter().HealthPotions > 0)
		{
			PlaySoundEffect(AudioData.MiniGameSound.Potion);
			GetCurrentGameCharacterDisplay().OnDoAction();
			_anim.Play("HealthPotion");
			GetCurrentGameCharacter().ModHitPoints(GetCurrentGameCharacter().Stats[GameCharacter.CharacterStat.TotalHitPoints]/2);
			GetCurrentGameCharacter().HealthPotions -= 1;
			ShowActionEndTurn(String.Format("{0} uses a health potion!", GetCurrentGameCharacter().CharacterName), 3);
		}
		_playerDisplay.UpdateDisplay();		
		_opponentDisplay.UpdateDisplay();
		_cntPotions.UpdatePotionButtons(_playerDisplay.CurrentCharacter.HealthPotions, _playerDisplay.CurrentCharacter.ManaPotions);
	}


	private void OnManaPotionPressed()
	{
		if (GetCurrentGameCharacter().ManaPotions > 0)
		{
			PlaySoundEffect(AudioData.MiniGameSound.Potion);
			GetCurrentGameCharacterDisplay().OnDoAction();
			_anim.Play("ManaPotion");
			GetCurrentGameCharacter().ModMana(GetCurrentGameCharacter().Stats[GameCharacter.CharacterStat.TotalMana]/2);
			GetCurrentGameCharacter().ManaPotions -= 1;
			ShowActionEndTurn(String.Format("{0} uses a mana potion!", GetCurrentGameCharacter().CharacterName), 3);
		}
		_playerDisplay.UpdateDisplay();		
		_opponentDisplay.UpdateDisplay();
		_cntPotions.UpdatePotionButtons(_playerDisplay.CurrentCharacter.HealthPotions, _playerDisplay.CurrentCharacter.ManaPotions);
	}

	private async void OnSpellPressed(string spellName)
	{
		Spell spell = SpellPool.Find(x => x.SpellName == spellName);

		if (GetCurrentGameCharacter().Stats[GameCharacter.CharacterStat.Mana] < spell.ManaCost)
		{
			ShowAction("Insufficient mana!",2.5f);

			await ToSignal(_actionTimer, "timeout");
			OnCntAbilitiesBtnBackPressed();
			return;
		}

		GetCurrentGameCharacterDisplay().OnDoAction();
		GetCurrentGameCharacter().ModMana(-spell.ManaCost);

		_anim.Play(spellName);

		int magnitude = 0;

		if (spell.TargetSelf)
		{
			magnitude = GetCurrentGameCharacter().TakeSpellEffect(spell, GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Sorcery],
			GetCurrentGameCharacter().GetLuckyNumber());
		}
		else
		{
			magnitude = GetCurrentOpponentGameCharacter().TakeSpellEffect(spell, GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Sorcery],
			GetCurrentGameCharacter().GetLuckyNumber());
			if (spell.Damage > 0)
			{
				TakeDamageAnim(GetOpposingGameCharacterDisplay());
			}
			if (spell.Damage > 0 && spell.DisableDuration == 0 && GetCurrentOpponentGameCharacter().IsAfflictedWithSleep())
			{
				ShowAction(String.Format("{0} wakes up!", GetCurrentOpponentGameCharacter().CharacterName),3f);
				await ToSignal(_actionTimer, "timeout");
			}
		}
		ShowActionEndTurn(GetSpellEffectText(spell, magnitude), 2.5f);
		// _playerDisplay.UpdateDisplay();		
		// _opponentDisplay.UpdateDisplay();
	}

	private string GetSpellEffectText(Spell spell, int magnitude)
	{
		string output = "";
		switch (spell.SpellName)
		{
			case ("Heart Bolt"):
				output = String.Format("Heart Bolt hits {0} for {1} damage!", GetCurrentOpponentGameCharacter().CharacterName, magnitude);
				PlaySoundEffect(AudioData.MiniGameSound.HeartBolt);
				break;
			case ("Transfusion"):
				output = String.Format("Transfusion heals {0} for {1} hit points!", GetCurrentGameCharacter().CharacterName, -magnitude);
				PlaySoundEffect(AudioData.MiniGameSound.Transfusion);
				break;
			case ("Plague"):
				output = String.Format("{0} casts plague on {1}!", GetCurrentGameCharacter().CharacterName, GetCurrentOpponentGameCharacter().CharacterName, magnitude);
				PlaySoundEffect(AudioData.MiniGameSound.Plague);
				break;
			case ("Sedation"):
				output = String.Format("{0} casts sedation on {1}!", GetCurrentGameCharacter().CharacterName, GetCurrentOpponentGameCharacter().CharacterName, magnitude);
				PlaySoundEffect(AudioData.MiniGameSound.Sedation);
				break;
			case ("Delirium"):
				output = String.Format("{0} casts delirium on {1}!", GetCurrentGameCharacter().CharacterName, GetCurrentOpponentGameCharacter().CharacterName, magnitude);
				PlaySoundEffect(AudioData.MiniGameSound.Delirium);
				break;
			case ("Recuperate"):
				output = String.Format("{0} casts recuperate!", GetCurrentGameCharacter().CharacterName, -magnitude);
				PlaySoundEffect(AudioData.MiniGameSound.Recuperate);
				break;
			case ("Bedlam"):
				output = String.Format("{0} casts bedlam on {1}!", GetCurrentGameCharacter().CharacterName, GetCurrentOpponentGameCharacter().CharacterName, magnitude);
				PlaySoundEffect(AudioData.MiniGameSound.Bedlam);
				break;
			case ("Magical Barrier"):
				output = String.Format("{0}'s magical defences are boosted!", GetCurrentGameCharacter().CharacterName);
				PlaySoundEffect(AudioData.MiniGameSound.MagicalBarrier);
				break;
			case ("Arcane Law"):
				output = String.Format("{0} summons a mystical lawyer to ward off legal trickery!", GetCurrentGameCharacter().CharacterName);
				PlaySoundEffect(AudioData.MiniGameSound.ArcaneLaw);
				break;
			case ("Grey Soul"):
				output = String.Format("{0} casts a shadow over their sense of humour...", GetCurrentGameCharacter().CharacterName);
				PlaySoundEffect(AudioData.MiniGameSound.GreySoul);
				break;
			case ("Coma"):
				output = String.Format("{0} casts coma on {1}!", GetCurrentGameCharacter().CharacterName, GetCurrentOpponentGameCharacter().CharacterName, magnitude);
				PlaySoundEffect(AudioData.MiniGameSound.Coma);
				break;
			case ("Flesh Wound"):
				output = String.Format("Flesh Wound hits {0} for {1} damage! Barely a scratch!", GetCurrentOpponentGameCharacter().CharacterName, magnitude);
				PlaySoundEffect(AudioData.MiniGameSound.FleshWound);
				break;
			case ("Default"):
				output = "text not yet implemented. if this makes it to the final build please @hnuqweasd#3731 on discord...";
				break;
		}
		return output;
	}

	private void OnBtnSpellsPressed()
	{
		ShowOneControlAreaContainer(_cntAbilities);
	}

	private void OnCntAbilitiesBtnBackPressed()
	{
		ShowOneControlAreaContainer(_cntControls);
	}

	private async void ShowActionEndTurn(string text, float speed = 3)
	{
		ShowAction(text,speed);
		_playerDisplay.UpdateDisplay();		
		_opponentDisplay.UpdateDisplay();

		await ToSignal(_actionTimer, "timeout");

		if (IsThereVictor())
		{
			return;
		}

		// GD.Print("Turn is about to be ended");
		
		_turnManager.EndTurn();

		OnEndingTurnCommon(GetCurrentGameCharacter());

	}

	private bool IsThereVictor()
	{
		if (_playerDisplay.CurrentCharacter.Stats[GameCharacter.CharacterStat.HitPoints] <= 0)
		{
			OnPlayerLost();
			return true;
		}
		else if (_opponentDisplay.CurrentCharacter.Stats[GameCharacter.CharacterStat.HitPoints] <= 0)
		{
			OnPlayerWon();
			return true;
		}
		return false;
	}

	private async void OnPlayerLost()
	{
		ShowAction("You have been defeated!",3f);

		await ToSignal(_actionTimer, "timeout");
		_lblAction.Text = "";
		EmitSignal(nameof(PlayerLost));
	}

	private async void OnPlayerWon()
	{
		ShowAction("You have defeated your opponent!",3f);

		await ToSignal(_actionTimer, "timeout");

		if (_rand.Next(2) == 0)
		{
			_playerDisplay.CurrentCharacter.HealthPotions += 1;
			ShowAction("You have found a health potion!",3f);
			await ToSignal(_actionTimer, "timeout");
		}
		if (_rand.Next(2) == 0)
		{
			_playerDisplay.CurrentCharacter.ManaPotions += 1;
			ShowAction("You have found a mana potion!",3f);
			await ToSignal(_actionTimer, "timeout");
		}

		int beforeLevel = _playerDisplay.CurrentCharacter.Stats[GameCharacter.CharacterStat.Level];

		// _playerDisplay.CurrentCharacter.Stats[GameCharacter.CharacterStat.Experience] += _opponentDisplay.CurrentCharacter.GetXPValue();
		_playerDisplay.CurrentCharacter.UpdateExperience(_opponentDisplay.CurrentCharacter.GetXPValue());
		_playerDisplay.UpdateDisplay();		

		ShowAction(String.Format("You have gained {0} experience!", _opponentDisplay.CurrentCharacter.GetXPValue()),3f);

		await ToSignal(_actionTimer, "timeout");

		if (_playerDisplay.CurrentCharacter.Stats[GameCharacter.CharacterStat.Level] > beforeLevel)
		{
			ShowAction(String.Format("Your power grows! You are now level {0}!",_playerDisplay.CurrentCharacter.Stats[GameCharacter.CharacterStat.Level]), 3f); 
			await ToSignal(_actionTimer, "timeout");
		}

		_lblAction.Text = "";
		EmitSignal(nameof(PlayerWin));
	}

	// private void PlayerDefeat()
	// {

	// }

	// private void OpponentDefeat()
	// {

	// }

	private void ShowAction(string text, float duration)
	{
		_lblAction.PercentVisible = 0;
		_actionTextAnim.PlaybackSpeed = 1/(duration-2);
		_actionTextAnim.Play("ShowText");
		_actionTimer.WaitTime = duration;
		_actionTimer.Start();
		_lblAction.Text = text;
		ShowOneControlAreaContainer(_cntAction);
	}

	private GameCharacter GetCurrentGameCharacter()
	{
		return ((GameCharacterDisplay) _turnManager.CurrentUnit).CurrentCharacter;
	}

	private GameCharacter GetCurrentOpponentGameCharacter()
	{
		return GetCurrentGameCharacter().Player ? _opponentDisplay.CurrentCharacter : _playerDisplay.CurrentCharacter;
	}
	
	private bool IsCurrentCharacterPlayer()
	{
		return ((GameCharacterDisplay) _turnManager.CurrentUnit).CurrentCharacter.Player;
	}

	public async void OnPlayerStartingTurn()
	{
		// if (GetCurrentGameCharacter().Disabled)
		// {
		// 	EndTurnDisabled();
		// 	return;
		// }
		// if (GetCurrentGameCharacter().Confused)
		// {
		// 	EndTurnConfused();
		// 	return;
		// }
		// GetCurrentGameCharacter().IndemnityBuff = GetCurrentGameCharacter().SolemnityBuff = GetCurrentGameCharacter().ResistanceBuff = false;

		


		if (GetCurrentGameCharacter().Disabled)
		{
			if (_rand.Next(0, GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]/10 + 2) != 0)
			{
				GetCurrentGameCharacter().ClearSleepEffects();
				ShowAction(GetCurrentGameCharacter().CharacterName + " wakes up!", 3f);
				// return;
				await ToSignal(_actionTimer, "timeout");
				
			}
			else
			// if (_rand.Next(0,(GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]/3)+2) == 0)
			{
				EndTurnDisabled();
				return;
			}
		}
		if (GetCurrentGameCharacter().Confused)
		{
			if (_rand.Next(0, GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]/5 + 2) != 0)
			{
				GetCurrentGameCharacter().ClearConfusionEffects();
				ShowAction(GetCurrentGameCharacter().CharacterName + " snaps out of confusion!", 3f);

				await ToSignal(_actionTimer, "timeout");
			}
			else
			// if (_rand.Next(0,(GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]/3)+2) == 0)
			{
				EndTurnConfused();
				return;
			}
		}
		_playerDisplay.UpdateDisplay();		
		_opponentDisplay.UpdateDisplay();
		ShowOneControlAreaContainer(_cntControls);
	}

	public async void OnOpponentStartingTurn()
	{
		// GetCurrentGameCharacter().IndemnityBuff = GetCurrentGameCharacter().SolemnityBuff = GetCurrentGameCharacter().ResistanceBuff = false;
		// ShowActionNextTurn();
		ShowAction(GetCurrentGameCharacter().CharacterName + "'s turn!", 3f);
		await ToSignal(_actionTimer, "timeout");

		if (GetCurrentGameCharacter().Disabled)
		{
			if (_rand.Next(0, GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]/10 + 2) != 0)
			{
				GetCurrentGameCharacter().ClearSleepEffects();
				ShowAction(GetCurrentGameCharacter().CharacterName + " wakes up!", 3f);
				// return;
				await ToSignal(_actionTimer, "timeout");
				
			}
			else
			// if (_rand.Next(0,(GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]/3)+2) == 0)
			{
				EndTurnDisabled();
				return;
			}
		}
		if (GetCurrentGameCharacter().Confused)
		{
			if (_rand.Next(0, GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]/5 + 2) != 0)
			{
				GetCurrentGameCharacter().ClearConfusionEffects();
				ShowAction(GetCurrentGameCharacter().CharacterName + " snaps out of confusion!", 3f);

				await ToSignal(_actionTimer, "timeout");
			}
			else
			// if (_rand.Next(0,(GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]/3)+2) == 0)
			{
				EndTurnConfused();
				return;
			}
		}

		_playerDisplay.UpdateDisplay();		
		_opponentDisplay.UpdateDisplay();
		
		var priorities = new Dictionary<GameCharacter.CharacterAttribute, int>() {
			{GameCharacter.CharacterAttribute.Humour,GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Humour] },
			{GameCharacter.CharacterAttribute.LegalTrickery,GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.LegalTrickery] },
			{GameCharacter.CharacterAttribute.Sorcery,GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Sorcery] }
		};

		GameCharacter.CharacterAttribute keyOfMaxValue = priorities.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;

		if (GetCurrentGameCharacter().HealthPotions > 0 &&
			(GetCurrentGameCharacter().Stats[GameCharacter.CharacterStat.HitPoints] < GetCurrentGameCharacter().Stats[GameCharacter.CharacterStat.TotalHitPoints]/2))
		{
			OnHealthPotionPressed();	
		}
		else if (GetCurrentGameCharacter().ManaPotions > 0 &&
			(GetCurrentGameCharacter().Stats[GameCharacter.CharacterStat.Mana] < GetCurrentGameCharacter().Stats[GameCharacter.CharacterStat.TotalMana]/2))
		{
			OnManaPotionPressed();	
		}
		else
		{
			AIChooseAttack(keyOfMaxValue);
		}
	}

	public void AIChooseAttack(GameCharacter.CharacterAttribute highestAttribute)
	{
		if (highestAttribute == GameCharacter.CharacterAttribute.Sorcery && GetCurrentGameCharacter().CannotAffordAnySpell())
		{
			AIChooseAttack(GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Humour] > 
				GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.LegalTrickery] ? GameCharacter.CharacterAttribute.Humour
				: GameCharacter.CharacterAttribute.LegalTrickery);
			return;
		}
		if (highestAttribute == GameCharacter.CharacterAttribute.Sorcery)
		{
			string spell = GetCurrentGameCharacter().GetRandomAppropriateSpellThatCanAfford();
			if (spell == "")
			{
				GD.Print("CAUGHT");
				AIChooseAttack(GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Humour] > 
					GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.LegalTrickery] ? GameCharacter.CharacterAttribute.Humour
					: GameCharacter.CharacterAttribute.LegalTrickery);
				return;
			}
			OnSpellPressed(spell);
		}
		else if (highestAttribute == GameCharacter.CharacterAttribute.Humour)
		{
			OnBtnTellJokePressed();
		}
		else if (highestAttribute == GameCharacter.CharacterAttribute.LegalTrickery)
		{
			OnBtnLawsuitPressed();
		}
	}

	public void EndTurnDisabled()
	{
		// GetCurrentGameCharacter().Disabled = false;


		ShowActionEndTurn(GetCurrentGameCharacter().CharacterName + " is asleep!", 2.5f);
		// _playerDisplay.UpdateDisplay();		
		// _opponentDisplay.UpdateDisplay();
	}

	public async void EndTurnConfused()
	{
		// GetCurrentGameCharacter().Confused = false;
		
		ShowAction(GetCurrentGameCharacter().CharacterName + " is confused!", 2.5f);
		await ToSignal(_actionTimer, "timeout");
		if (_rand.Next(1 + GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]) == 0)
		{
			GetCurrentGameCharacter().DefendAgainstPureDamage
			(_rand.Next(1, Math.Max(3, 6-GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck])));
			ShowActionEndTurn(GetCurrentGameCharacter().CharacterName + " hurts itself!", 2.5f);
		}
		else
		{
			GetCurrentOpponentGameCharacter().DefendAgainstPureDamage
			(_rand.Next(1, Math.Max(GetCurrentGameCharacter().Attributes[GameCharacter.CharacterAttribute.Luck]/2,3)));
			ShowActionEndTurn(GetCurrentGameCharacter().CharacterName + " goes berserk!", 2.5f);
		}
		// await ToSignal(_actionTimer, "timeout");
		// _playerDisplay.UpdateDisplay();		
		// _opponentDisplay.UpdateDisplay();
	}

	public void OnPlayerEndingTurn()
	{
		// foreach (DurationEffect effect in _playerDisplay.CurrentCharacter.CurrentEffects)
		// {
		// 	_playerDisplay.CurrentCharacter.DoDurationEffect(effect);
		// 	ShowAction(_playerDisplay.CurrentCharacter.CharacterName + " is affected by " + effect.EffectName + "!", 2);
		// 	await ToSignal(_actionTimer, "timeout");
		// }
		// // _playerDisplay.CurrentCharacter.DoAllDurationEffects();
		// if (IsThereVictor())
		// {
		// 	_lblAction.Text = "";
		// 	return;
		// }
		// OnEndingTurnCommon(_playerDisplay.CurrentCharacter);
	}

	private async void OnEndingTurnCommon(GameCharacter gameCharacter)
	{
		// if (_actionTimer.TimeLeft > 0)
		// {
		// 	await ToSignal(_actionTimer, "timeout");
		// }
		GD.Print("have ended the turn of " + gameCharacter.CharacterName);
		gameCharacter.ResetStatusEffects();
		foreach (DurationEffect effect in gameCharacter.CurrentEffects.ToList())
		{
			GD.Print(effect.EffectName);
			gameCharacter.DoDurationEffect(effect);
			if (effect.Damage > 0)
			{
				GetCurrentGameCharacterDisplay().OnTakeDamage();
			}
			ShowAction(gameCharacter.CharacterName + " is affected by " + effect.EffectName + "!", 2.5f);
			_playerDisplay.UpdateDisplay();		
			_opponentDisplay.UpdateDisplay();
			await ToSignal(_actionTimer, "timeout");
		}
		// _playerDisplay.CurrentCharacter.DoAllDurationEffects();
		if (IsThereVictor())
		{
			_lblAction.Text = "";
			return;
		}

		NextTurn();
	}

	public void OnOpponentEndingTurn()
	{
		// OnEndingTurnCommon(_opponentDisplay.CurrentCharacter);
	}

	private void ShowActionNextTurn()
	{
		
		// await ToSignal(_actionTimer, "timeout");
	}

	// private void OnPlayerTurn()
	// {
	// 	// when finished.. 
	// 	// _turnManager.EndTurn();
	// 	// can do stuff
	// 	// OnTurnFinished();
	// 	_turnManager.EndTurn();
	// }

	private void NextTurn()
	{
		// if (!GetCurrentGameCharacter().Player)
		// {
		// 	ShowAction(GetCurrentGameCharacter().CharacterName + "'s turn!",2);
			// await ToSignal(_actionTimer, "timeout");
		// }

		_turnManager.NextTurn();
		GD.Print(((GameCharacterDisplay) _turnManager.CurrentUnit).CurrentCharacter.CharacterName + "'s turn");
		GD.Print(((GameCharacterDisplay) _turnManager.CurrentUnit).CurrentCharacter.CharacterName + "'s initiative is " +
			((GameCharacterDisplay) _turnManager.CurrentUnit).CurrentCharacter.Attributes[GameCharacter.CharacterAttribute.Initiative]);
		// if (((GameCharacterDisplay) _turnManager.CurrentUnit).CurrentCharacter.Player)
		// {
		// 	OnPlayerTurn();
		// }
		// else
		// {
		// 	OnOpponentTurn();
		// }
	}

	// private void OnOpponentTurn()
	// {
	// 	// do stuff
	// 	// then end turn
	// 	_turnManager.EndTurn();
	// }



}
