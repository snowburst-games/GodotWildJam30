using Godot;
using System;

public class CntPotions : Control
{
	private Button _healthPotion;
	private Button _manaPotion;

	public override void _Ready()
	{
		_healthPotion = GetNode<Button>("HBox/HealthPotion");
		_manaPotion = GetNode<Button>("HBox/ManaPotion");
	}

	public void UpdatePotionButtons(int healthPots, int manaPots)
	{
		_healthPotion.Text = String.Format("Health Potions ({0})", healthPots);
		_healthPotion.Disabled = healthPots == 0;
		_manaPotion.Text = String.Format("Mana Potions ({0})", manaPots);
		_manaPotion.Disabled = manaPots == 0;
	}

}
