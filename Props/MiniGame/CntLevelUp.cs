using Godot;
using System;
using System.Collections.Generic;

public class CntLevelUp : Control
{
	[Signal]
	public delegate void BtnSkillPressed(GameCharacter.CharacterAttribute attr);

	private Label _lblTitle;
	private Dictionary<GameCharacter.CharacterAttribute, Button> _attributeButtons;

	private Dictionary<GameCharacter.CharacterAttribute, string> _attributeText = new Dictionary<GameCharacter.CharacterAttribute, string>()
	{
		{GameCharacter.CharacterAttribute.Sorcery, "Sorcery"},
		{GameCharacter.CharacterAttribute.Resistance, "Resistance"},
		{GameCharacter.CharacterAttribute.Humour, "Humour"},
		{GameCharacter.CharacterAttribute.Solemnity, "Solemnity"},
		{GameCharacter.CharacterAttribute.LegalTrickery, "Legal Trickery"},
		{GameCharacter.CharacterAttribute.Indemnity, "Indemnity"},
		{GameCharacter.CharacterAttribute.Initiative, "Initiative"},
		{GameCharacter.CharacterAttribute.Luck, "Luck"},
	};

	public override void _Ready()
	{
		_lblTitle = GetNode<Label>("LblTitle");
		_attributeButtons = new Dictionary<GameCharacter.CharacterAttribute, Button>() {
			{GameCharacter.CharacterAttribute.Sorcery, GetNode<Button>("VBox/HBox/BtnSorcery")},
			{GameCharacter.CharacterAttribute.Resistance, GetNode<Button>("VBox/HBox/BtnResistance")},
			{GameCharacter.CharacterAttribute.Humour, GetNode<Button>("VBox/HBox2/BtnHumour")},
			{GameCharacter.CharacterAttribute.Solemnity, GetNode<Button>("VBox/HBox2/BtnSolemnity")},
			{GameCharacter.CharacterAttribute.LegalTrickery, GetNode<Button>("VBox/HBox3/BtnLegal")},
			{GameCharacter.CharacterAttribute.Indemnity, GetNode<Button>("VBox/HBox3/BtnIndemnity")},
			{GameCharacter.CharacterAttribute.Initiative, GetNode<Button>("VBox/HBox4/BtnInitiative")},
			{GameCharacter.CharacterAttribute.Luck, GetNode<Button>("VBox/HBox4/BtnLuck")},
		};
		foreach (KeyValuePair<GameCharacter.CharacterAttribute, Button> kv in _attributeButtons)
		{
			kv.Value.Connect("pressed", this, nameof(OnBtnSkillPressed), new Godot.Collections.Array() {kv.Key});
		}
	}

	public void UpdateTitle(int skillPointsRemaining)
	{
		_lblTitle.Text = String.Format("Spend your skill points ({0} remaining): ", skillPointsRemaining);
	}

	public void SetLabel(GameCharacter.CharacterAttribute attribute, int number)
	{
		_attributeButtons[attribute].Text = String.Format("{0} ({1})", _attributeText[attribute], number);
	}

	public void OnBtnSkillPressed(GameCharacter.CharacterAttribute attribute)
	{
		EmitSignal(nameof(BtnSkillPressed), attribute);
	}
}
