using Godot;
using System;
using System.Collections.Generic;
public class CntAbilities : Control
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private GridContainer _gridContainer;
	public List<Button> SpellButtons;
	private PackedScene _scnBtnBase;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_scnBtnBase = GD.Load<PackedScene>("res://Interface/Buttons/BtnBase/BtnBase.tscn");
		_gridContainer = GetNode<GridContainer>("HBox/Grid");
		SpellButtons = new List<Button>() {
			GetNode<Button>("HBox/VBox/HBox/Spell1"),
			GetNode<Button>("HBox/VBox/HBox/Spell2"),
			GetNode<Button>("HBox/VBox/HBox2/Spell3"),
			GetNode<Button>("HBox/VBox/HBox2/Spell4")
		};
	}

	public void UpdateSpellButtons(List<Spell> spells)
	{
		foreach (Button btn in SpellButtons)
		{
			btn.QueueFree();
		}
		SpellButtons.Clear();

		foreach (Spell spell in spells)
		{
			AddButton(spell);
		}

		// // GD.Print("number of spells: ", spells.Count);
		// foreach (Button b in SpellButtons)
		// {
		// 	Visible = false;
		// }
		// for (int i = 0; i < spells.Count; i++)
		// {
		// 	// if (spells.Count < i+1)
		// 	// {
		// 	// 	SpellButtons[i].Visible = false;
		// 	// 	continue;
		// 	// }
		// 	SpellButtons[i].Visible = true;
		// 	SpellButtons[i].Text = String.Format("{0} (Cost: {1})",spells[i].SpellName, spells[i].ManaCost);
		// 	SpellButtons[i].HintTooltip = spells[i].Tooltip;
		// 	SpellButtons[i].SetMeta("Spell", spells[i].SpellName);
		// }
	}
	// 	foreach (Spell spell in spells)
	// 	{
	// 		AddButton(spell);
	// 		if (knownSpells.Contains(spell))
	// 		{
	// 			OnSpellLearned(spell);
	// 		}
	// 	}
	// 		// SpellButtons[i].SetMeta("Spell", spells[i].SpellName);
	// }

	private void AddButton(Spell spell)
	{
		// foreach (Button btn in SpellButtons)
		// {
		// 	if (btn.Text == spell.SpellName)
		// 	{
		// 		return;
		// 	}
		// }
		BtnBase btnBase = (BtnBase) _scnBtnBase.Instance();
		btnBase.SetMeta("Spell", spell.SpellName);
		btnBase.Text = String.Format("{0} (Cost: {1})",spell.SpellName, spell.ManaCost);
		btnBase.HintTooltip = spell.Tooltip;
		btnBase.SizeFlagsHorizontal = (int) SizeFlags.ExpandFill;
		btnBase.SizeFlagsVertical = (int) SizeFlags.ExpandFill;
		_gridContainer.AddChild(btnBase);
		SpellButtons.Add(btnBase);
	}

}
