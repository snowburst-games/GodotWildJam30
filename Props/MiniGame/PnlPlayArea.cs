using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class PnlPlayArea : Panel
{

	[Signal]
	public delegate void HeartRateModified(int amount);
	
	private CntWelcome _cntWelcome;
	private CntBattle _cntBattle;
	private CntLevelUp _cntLevelUp;
	private CntLearnSpells _cntLearnSpells;

	// private Control _cntCharacters;
	private AnimationPlayer _anim;
	private Label _lblAnnounce;

	private List<Spell> _startingSpellPool = new List<Spell>()
	{
		new Spell() {SpellName = "Heart Bolt", Damage = 3, ManaCost = 2, Tooltip = "Fire a mystical bolt directly at the target's heart."},
		new Spell() {SpellName = "Transfusion", Damage = -5, ManaCost = 2, TargetSelf = true, Tooltip = "Sneak in a blood transfusion to heal yourself when nobody is looking..."},
		new Spell() {SpellName = "Sedation", DisableDuration = 3, ManaCost = 3, Tooltip = "Throw a ketamine dart at the opponent."},
		new Spell() {SpellName = "Delirium", ConfuseDuration = 3, ManaCost = 3, Tooltip = "Confuse the opponent. Results may vary."},
		new Spell() {SpellName = "Magical Barrier", BuffResistanceDuration = 3, TargetSelf = true, ManaCost = 2, Tooltip = "Boost your magical defences."},
		new Spell() {SpellName = "Arcane Law", BuffIndemnityDuration = 3, TargetSelf = true, ManaCost = 2, Tooltip = "Summon mystical phantom lawyers to boost your indemnity."},
		new Spell() {SpellName = "Grey Soul", BuffSolemnityDuration = 3, TargetSelf = true, ManaCost = 2, Tooltip = "Banish your sense of humour to boost your solemnity."},
		new Spell() {SpellName = "Flesh Wound", Damage = 2, ManaCost = 1, Tooltip = "A flesh wound! Your target probably won't even notice."}
	};
	private List<Spell> _levelUpSpellPool = new List<Spell>()
	{
		new Spell() {SpellName = "Plague", Damage = 1, DamageDuration = 3, ManaCost = 1, Tooltip = "Feed the target some petri dishes..."},
		new Spell() {SpellName = "Coma", DisableDuration = 4, ManaCost = 5, Damage = 1, Tooltip = "Induce a \"therapeutic\" coma to put your target to sleep."},
		new Spell() {SpellName = "Recuperate", Damage = -2, ManaCost = 3, TargetSelf = true, DamageDuration = 3, Tooltip = "These medicines are effective, but they take time."},
		new Spell() {SpellName = "Bedlam", ConfuseDuration = 4, ManaCost = 5, Damage = 1, DamageDuration = 3, Tooltip = "Steal away the target's sanity. Results may vary."}
	};

	private List<Spell> _allSpells;

	private List<GameCharacter> _opponents = new List<GameCharacter>();

	private List<GameCharacter> _defeatedOpponents = new List<GameCharacter>();

	private string _playerName = "Player";
	private bool _startingGame = true;

	public string PlayerName {
		get {
			return _playerName;
		}
		set {
			_playerName = value;
			if (_player != null)
			{
				_player.CharacterName = value;
			}
			// need also to update any player labels!
		}
	}
	private GameCharacter _player;
	private GameCharacter _currentOpponent;

	private int _characterCount = 0;
	private int _startingLevel = 1;
	private int _maxSpells = 1;
	
	public override void _Ready()
	{
		// _cntCharacters = GetNode<Control>("CntCharacters");
		_cntWelcome = GetNode<CntWelcome>("CntWelcome");
		_cntBattle = GetNode<CntBattle>("CntBattle");
		_cntLearnSpells = GetNode<CntLearnSpells>("CntLearnSpells");
		var spellList = _startingSpellPool.Concat(_levelUpSpellPool);
		_allSpells = spellList.ToList();//_startingSpellPool.ToList();// .AddRange(_levelUpSpellPool.ToList()); // (List<Spell>) _startingSpellPool.Concat(_levelUpSpellPool);
		// foreach (Spell spell in _levelUpSpellPool)
		// {
		// 	_allSpells.Add(spell);
		// }
		_cntBattle.SpellPool = _allSpells;
		_cntLevelUp = GetNode<CntLevelUp>("CntLevelUp");
		_anim = GetNode<AnimationPlayer>("Anim");
		_lblAnnounce = GetNode<Label>("PnlAnnounce/Label");
		ConnectPlayAreaSignals();
		ShowOnePlayAreaContainer(_cntWelcome);

	}

	private void ConnectPlayAreaSignals()
	{
		_cntLevelUp.Connect(nameof(CntLevelUp.BtnSkillPressed), this, nameof(OnBtnSkillPressed));
		_cntBattle.Connect(nameof(CntBattle.PlayerWin), this, nameof(OnOpponentDefeated));
		_cntBattle.Connect(nameof(CntBattle.PlayerLost), this, nameof(OnPlayerDefeated));
	}

	private void GenerateOpponents()
	{
		_characterCount += 1;
		// _startingLevel += 1;
		List<DialogueControl.CharacterEnum> characters = Enum.GetValues(typeof(DialogueControl.CharacterEnum)).Cast<DialogueControl.CharacterEnum>().ToList();
		foreach (DialogueControl.CharacterEnum ch in characters)
		{
			GameCharacter opponent = new GameCharacter(
				uid:_characterCount,
				name: ch.ToString(),
				level: _startingLevel,
				player:false,
				dialogueCharacter:ch
			);
			
			opponent.SetPotionsByLevel();
			opponent.LearnRandomSpells(_allSpells);
			_opponents.Add(opponent);
			_characterCount += 1;
			_startingLevel += 1;
			// _cntCharacters.AddChild(opponent);
			opponent.SpendSkillPoints();
		}
		_opponents.Sort((x, y) => x.Stats[GameCharacter.CharacterStat.Level].CompareTo(y.Stats[GameCharacter.CharacterStat.Level]));
	}

	private void ShowOnePlayAreaContainer(Control playAreaContainer)
	{
		_cntWelcome.Visible = false;
		_cntBattle.Visible = false;
		_cntLevelUp.Visible = false;
		_cntLearnSpells.Visible = false;
		playAreaContainer.Visible = true;
	}

	private void GeneratePlayer()
	{
		_player = new GameCharacter(
			uid: _characterCount,
			name: _playerName,
			level: 1,
			player: true,
			dialogueCharacter:0
		);
		_player.ManaPotions = 1;
		_player.HealthPotions = 1;
		// _player.LearnRandomSpells(_startingSpellPool); // possible extension - at the start of the game, allow the player to pick 4 spells.
		// _cntCharacters.AddChild(_player);
		_player.Connect(nameof(GameCharacter.PlayerSpendingSkillPoints), this, nameof(OnPlayerSpendingSkillPoints));
		_player.SpendSkillPoints();
	}

	public void Start()
	{
		// foreach (Node n in _cntCharacters.GetChildren())
		// {
		// 	n.QueueFree();
		// }
		_maxSpells = 1;
		_startingLevel = 1;
		_player = null;
		_currentOpponent = null;
		_startingGame = true;
		_opponents.Clear();
		_defeatedOpponents.Clear();
		GenerateOpponents();
		GeneratePlayer();
	}

	public void NextOpponent()
	{
		_currentOpponent.ResetCharacter();
	}

	private async void StartOpponent()
	{

		_currentOpponent = _opponents[0];
		_currentOpponent.ResetCharacter();
		_player.ResetCharacter();
		_cntBattle.SetCharacterDisplay(_currentOpponent);
		_cntBattle.SetCharacterDisplay(_player);

		_lblAnnounce.Text = String.Format("{0} vs {1}! GO!", _player.CharacterName, _currentOpponent.CharacterName);
		_anim.Play("Announce");
		_startingGame = false;

		await ToSignal(_anim, "animation_finished");

		_cntBattle.StartBattle();
	}

	public void OnOpponentDefeated()
	{
		EmitSignal(nameof(HeartRateModified), -40);
		_defeatedOpponents.Add(_currentOpponent);
		_opponents.Remove(_currentOpponent);

		if (_opponents.Count == 0)
		{
			_defeatedOpponents.Clear();
			_startingLevel = _player.Stats[GameCharacter.CharacterStat.Level];
			GenerateOpponents();
		}
		if (_player.SkillPoints > 0)
		{
			WelcomeYouHaveSkillPoints();
		}
		else
		{
			WelcomeNextOpponent("You have won! Bless your artichoke heart! We've eased it's frantic pumping!\n\nWhy not try a stronger opponent?");
		}
		// emit a signal to decrease heart rate by n. also give a clue -- clue per opponent. show in the update text and emit for journal
		// StartOpponent();
	}

	public void WelcomeNextOpponent(string text)
	{
		ShowOnePlayAreaContainer(_cntWelcome);
		_cntWelcome.UpdateText(text);
		_cntWelcome.HideAllButtons();
		GetNode<Button>("CntWelcome/BtnReady").Visible = true;
	}
	
	public void WelcomeYouHaveSkillPoints()
	{
		ShowOnePlayAreaContainer(_cntWelcome);
		_cntWelcome.UpdateText(string.Format("Well done! You have accumulated {0} skill points!\n\nWhy not spend them?", _player.SkillPoints));
		_cntWelcome.HideAllButtons();
		GetNode<Button>("CntWelcome/BtnSkillUp").Visible = true;
	}

	private void OnBtnSkillUpPressed()
	{
		_player.SpendSkillPoints();
	}

	public void OnReadyPressed()
	{
		StartOpponent();
	}

	public void OnPlayerDefeated()
	{
		ShowOnePlayAreaContainer(_cntWelcome);
		_cntWelcome.UpdateText("You have lost! But you can always try again...\n\nThat is, if your heart can take it! LITERALLY! HA HA HA!");
		EmitSignal(nameof(HeartRateModified), 10);
		_cntWelcome.HideAllButtons();
		GetNode<Button>("CntWelcome/BtnReady").Visible = true;
		// emit a signal to increase heart rate by n
	}

	public void OnPlayerSpendingSkillPoints()
	{
		RefreshLevelUpPlayArea();
		ShowOnePlayAreaContainer(_cntLevelUp);
	}

	private void RefreshLevelUpPlayArea()
	{
		_cntLevelUp.UpdateTitle(_player.SkillPoints);
		foreach (GameCharacter.CharacterAttribute attr in _player.Attributes.Keys)
		{
			_cntLevelUp.SetLabel(attr, _player.Attributes[attr]);
		}
	}

	private void ChooseStartingSpells()
	{
		// List<Spell> availableSpells = _startingGame ? _startingSpellPool.ToList() : _allSpells.ToList();
		// foreach (Spell spell in availableSpells.ToList())
		// {
		// 	if (_player.KnownSpells.Contains(spell))
		// 	{
		// 		availableSpells.Remove(spell);
		// 	}
		// }
		_cntLearnSpells.PopulateGrid(_startingGame ? _startingSpellPool : _allSpells, _player.KnownSpells);
		
		foreach (Button btn in _cntLearnSpells.SpellButtons)
		{
			if (btn.IsConnected("pressed", this, nameof(OnLearnSpellPressed)))
			{
				btn.Disconnect("pressed", this, nameof(OnLearnSpellPressed));
			}
			btn.Connect("pressed", this, nameof(OnLearnSpellPressed), new Godot.Collections.Array{(string)btn.GetMeta("Spell")});
		}

		ShowOnePlayAreaContainer(_cntLearnSpells);
	}

	private void RefreshStartingSpells()
	{

	}


	public void OnLearnSpellPressed(string spellName)
	{
		_player.LearnSpell(_allSpells.Find(x => x.SpellName == spellName));
		if (_player.KnownSpells.Count == Math.Min(4,_maxSpells))
		{
			_cntBattle.SetCharacterDisplay(_player);
			WelcomeNextOpponent("Excellent! You've learned as much as your artichoke-addled mind can tolerate!\nLet's go!");
			_maxSpells = Math.Min(_maxSpells+1, 4);
		}
		else // if (_player.KnownSpells.Count < 4)
		{
			// ChooseStartingSpells();
			_cntLearnSpells.OnSpellLearned(_allSpells.Find(x => x.SpellName == spellName));
		}
	}

	public void OnBtnSkillPressed(GameCharacter.CharacterAttribute attr)
	{
		if (_player.SkillPoints > 0)
		{
			_player.Attributes[attr] += 1;
			_player.SkillPoints -= 1;
			RefreshLevelUpPlayArea();
		}
		if (_player.SkillPoints == 0)
		{
			if (_player.KnownSpells.Count == 4)
			{
				WelcomeNextOpponent("Now that you've learned something, it's time for a tougher opponent!\nLet's go!");
			}
			else
			{
				ChooseStartingSpells();
			}
			// if (_startingGame)
			// {
			// 	// StartOpponent();
			// 	ChooseStartingSpells();
			// }
			// else
			// {
			// 	ChooseStartingSpells(false);
			// 	// WelcomeNextOpponent("Now that you have learned something, why not test yourself against a tougher opponent?");
			// }
		}
	}

	private void OnAnnounceMidAnimation()
	{
		ShowOnePlayAreaContainer(_cntBattle);
	}

	private void OnBtnStartPressed()
	{
		Start();
	}
	private void OnPnlPlayAreaGUIInput(InputEvent ev)
	{
		if (ev is InputEventMouseButton evb)
		{
			if (evb.Pressed)
			{	// skip the waiting for timer thing
				GetNode<Timer>("CntBattle/PnlControlArea/CntAction/Timer").Stop();
				GetNode<Timer>("CntBattle/PnlControlArea/CntAction/Timer").EmitSignal("timeout");
			}
		}
	}


}
