using Godot;
using System;
using System.Linq;
using System.Collections.Generic;

public class GameCharacterDisplay : Node2D, ITurnBasedUnit
{
	private Label _lblName;
	private Label _lblLevel;
	private ProgressBar _progressHP;
	private ProgressBar _progressMana;
	private ProgressBar _progressXP;
	private Label _lblHP;
	private Label _lblMana;
	private Label _lblXP;
	public GameCharacter CurrentCharacter {get; set;}
	private Dictionary<DialogueControl.CharacterEnum, Texture> _gameCharacterTextures;


	[Signal]
	public delegate void StartingTurn();
	[Signal]
	public delegate void EndingTurn();

	public float Initiative {
		get{
			return CurrentCharacter.Attributes[GameCharacter.CharacterAttribute.Initiative];
		}
	}

	public override void _Ready()
	{
		_gameCharacterTextures = new Dictionary<DialogueControl.CharacterEnum, Texture>()
		{
			{DialogueControl.CharacterEnum.Queenie, GD.Load<Texture>("res://Actors/Thumbs/Queenie.png")},
			{DialogueControl.CharacterEnum.Marky, GD.Load<Texture>("res://Actors/Thumbs/Marky.png")},
			{DialogueControl.CharacterEnum.Paul, GD.Load<Texture>("res://Actors/Thumbs/Paul.png")},
			{DialogueControl.CharacterEnum.Freddie, GD.Load<Texture>("res://Actors/Thumbs/Freddie.png")}
			// {DialogueControl.CharacterEnum.tealc, GD.Load<Texture>("res://icon.png")}
		};
	}

	public void OnStartTurn()
	{
		EmitSignal(nameof(StartingTurn));
	}

	public void OnEndTurn()
	{
		GD.Print("Ending turn of " + CurrentCharacter.CharacterName);
		EmitSignal(nameof(EndingTurn));
	}

	public void Initialise(GameCharacter gameCharacter)
	{
		CurrentCharacter = gameCharacter;
		_lblName = GetNode<Label>("PnlInfo/VBox/HBoxName/LblName");
		_lblLevel = GetNode<Label>("PnlInfo/VBox/HBoxName/LblLevel");
		_progressHP = GetNode<ProgressBar>("PnlInfo/VBox/HBoxHP/BarHP");
		_lblHP = GetNode<Label>("PnlInfo/VBox/HBoxHP/BarHP/Label");
		_progressMana = GetNode<ProgressBar>("PnlInfo/VBox/HBoxMana/BarMana");
		_lblMana = GetNode<Label>("PnlInfo/VBox/HBoxMana/BarMana/Label");
		if (gameCharacter.Player)
		{
			_progressXP = GetNode<ProgressBar>("PnlInfo/VBox/HBoxXP/BarXP");
			_lblXP = GetNode<Label>("PnlInfo/VBox/HBoxXP/BarXP/Label");
		}
	}

	public void UpdateDisplay()
	{
		if (CurrentCharacter == null)
		{
			GD.Print("Need to initialise first.");
			throw new InvalidOperationException();
		}
		_lblName.Text = CurrentCharacter.CharacterName;
		_lblLevel.Text = "Level " + CurrentCharacter.Stats[GameCharacter.CharacterStat.Level];
		_lblHP.Text = String.Format("{0} / {1}", 
			CurrentCharacter.Stats[GameCharacter.CharacterStat.HitPoints], CurrentCharacter.Stats[GameCharacter.CharacterStat.TotalHitPoints]);
		_lblMana.Text = String.Format("{0} / {1}", 
			CurrentCharacter.Stats[GameCharacter.CharacterStat.Mana], CurrentCharacter.Stats[GameCharacter.CharacterStat.TotalMana]);
		_progressHP.Value = ((float)CurrentCharacter.Stats[GameCharacter.CharacterStat.HitPoints] / (float)CurrentCharacter.Stats[GameCharacter.CharacterStat.TotalHitPoints]) * 100;
		_progressMana.Value = ((float)CurrentCharacter.Stats[GameCharacter.CharacterStat.Mana] / (float)CurrentCharacter.Stats[GameCharacter.CharacterStat.TotalMana]) * 100;
		// GD.Print(CurrentCharacter.Stats[GameCharacter.CharacterStat.TotalHitPoints]);
		if (CurrentCharacter.Player)
		{
			// _progressXP.Value = ((float)CurrentCharacter.GetXPSinceThisLevel() / (float)CurrentCharacter.GetXPFromThisLevelToNextLevel()) * 100;
			_progressXP.Value = ((float)CurrentCharacter.Stats[GameCharacter.CharacterStat.Experience] / 
				(float)CurrentCharacter.GetExperienceNeededForLevel(CurrentCharacter.Stats[GameCharacter.CharacterStat.Level]+1)) * 100;
			_lblXP.Text = String.Format("{0} / {1}",  CurrentCharacter.Stats[GameCharacter.CharacterStat.Experience], 
				CurrentCharacter.GetExperienceNeededForLevel(CurrentCharacter.Stats[GameCharacter.CharacterStat.Level]+1));
		}

		UpdateStatusEffects();

		// CurrentCharacter.GetActiveAfflictions();
	}

	public void UpdateBestWorstIcons()
	{
		if (CurrentCharacter == null)
		{
			GD.Print("Need to initialise first.");
			throw new InvalidOperationException();
		}
		
		Dictionary<GameCharacter.CharacterAttribute, int> offensiveAttributes = new Dictionary<GameCharacter.CharacterAttribute, int>()
		{
			{GameCharacter.CharacterAttribute.Sorcery, CurrentCharacter.Attributes[GameCharacter.CharacterAttribute.Sorcery]},
			{GameCharacter.CharacterAttribute.LegalTrickery, CurrentCharacter.Attributes[GameCharacter.CharacterAttribute.LegalTrickery]},
			{GameCharacter.CharacterAttribute.Humour, CurrentCharacter.Attributes[GameCharacter.CharacterAttribute.Humour]}
		};
		Dictionary<GameCharacter.CharacterAttribute, int> defensiveAttributes = new Dictionary<GameCharacter.CharacterAttribute, int>()
		{
			{GameCharacter.CharacterAttribute.Resistance, CurrentCharacter.Attributes[GameCharacter.CharacterAttribute.Resistance]},
			{GameCharacter.CharacterAttribute.Indemnity, CurrentCharacter.Attributes[GameCharacter.CharacterAttribute.Indemnity]},
			{GameCharacter.CharacterAttribute.Solemnity, CurrentCharacter.Attributes[GameCharacter.CharacterAttribute.Solemnity]}
		};
		GameCharacter.CharacterAttribute bestAttribute = offensiveAttributes.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
		GameCharacter.CharacterAttribute worstAttribute = defensiveAttributes.Aggregate((x, y) => x.Value < y.Value ? x : y).Key;
		GetNode<StatusPanel>("PnlInfo/StatusPanel").SetBestWorstIcons(bestAttribute, worstAttribute);
	}

	public void UpdateStatusEffects()
	{
		if (CurrentCharacter == null)
		{
			GD.Print("Need to initialise first.");
			throw new InvalidOperationException();
		}
		List<StatusPanel.StatusEffect> statusEffects = new List<StatusPanel.StatusEffect>();
		if (CurrentCharacter.Confused)
		{
			statusEffects.Add(StatusPanel.StatusEffect.Confused);
		}
		if (CurrentCharacter.Disabled)
		{
			statusEffects.Add(StatusPanel.StatusEffect.Sleep);
		}
		if (CurrentCharacter.IsAfflictedWithDot())
		{
			statusEffects.Add(StatusPanel.StatusEffect.Dot);
		}
		if (CurrentCharacter.IsAfflictedWithHot())
		{
			statusEffects.Add(StatusPanel.StatusEffect.Hot);
		}
		if (CurrentCharacter.ResistanceBuff)
		{
			GD.Print(CurrentCharacter.CharacterName + " has a resistance buff.");
			statusEffects.Add(StatusPanel.StatusEffect.Resist);
		}
		if (CurrentCharacter.IndemnityBuff)
		{
			statusEffects.Add(StatusPanel.StatusEffect.Indemnity);
		}
		if (CurrentCharacter.SolemnityBuff)
		{
			statusEffects.Add(StatusPanel.StatusEffect.Solemnity);
		}
		GetNode<StatusPanel>("PnlInfo/StatusPanel").SetStatusEffects(statusEffects);
	}

	public void OnTakeDamage()
	{
		GetNode<AnimationPlayer>("Anim").Play("TakeDamage");
	}

	public void OnDoAction()
	{
		GetNode<AnimationPlayer>("Anim").Play("Action");
	}

	public void SetSprite()//DialogueControl.CharacterEnum gameCharacter)
	{
		if (CurrentCharacter == null)
		{
			GD.Print("Need to initialise first.");
			throw new InvalidOperationException();
		}
		if (CurrentCharacter.Player)
		{
			GetNode<Sprite>("Sprite").Texture = GD.Load<Texture>("res://Props/MiniGame/Art/Portraits/PLAYER.png");// path to player
			GetNode<Sprite>("Sprite").Scale = new Vector2(0.66f, 0.66f);
			return;
		}
		GetNode<Sprite>("Sprite").Texture = _gameCharacterTextures[CurrentCharacter.DialogueCharacter];
		GetNode<Sprite>("Sprite").Scale = new Vector2(0.41f, 0.41f);

	}

}
// CurrentCharacter.Stats[GameCharacter.CharacterStat.Experience].ToString()
