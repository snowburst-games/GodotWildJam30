using Godot;
using System;
using System.Collections.Generic;

public class StatusPanel : Panel
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	public enum StatusEffect { Dot, Hot, Sleep, Confused, Resist, Indemnity, Solemnity}

	// public enum AttributeBest { Legal, Humour, Sorcery}

	// public enum AttributeWorst { Indemnity, Solemnity, Resistance }

	private Dictionary<StatusEffect, TextureRect> _statusIcons;
	private Dictionary<GameCharacter.CharacterAttribute, TextureRect> _bestIcons;
	private Dictionary<GameCharacter.CharacterAttribute, TextureRect> _worstIcons;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_statusIcons = new Dictionary<StatusEffect, TextureRect>()
		{
			{ StatusEffect.Dot, GetNode<TextureRect>("StatusHBox/StatusDot")},
			{ StatusEffect.Hot, GetNode<TextureRect>("StatusHBox/StatusHot")},
			{ StatusEffect.Sleep, GetNode<TextureRect>("StatusHBox/StatusSleep")},
			{ StatusEffect.Confused, GetNode<TextureRect>("StatusHBox/StatusConfused")},
			{ StatusEffect.Resist, GetNode<TextureRect>("StatusHBox/StatusResist")},
			{ StatusEffect.Indemnity, GetNode<TextureRect>("StatusHBox/StatusIndemn")},
			{ StatusEffect.Solemnity, GetNode<TextureRect>("StatusHBox/StatusSolemn")}
		};
		_bestIcons = new Dictionary<GameCharacter.CharacterAttribute, TextureRect>()
		{
			{ GameCharacter.CharacterAttribute.LegalTrickery, GetNode<TextureRect>("BestWorstHBox/BestLegal")},
			{ GameCharacter.CharacterAttribute.Humour, GetNode<TextureRect>("BestWorstHBox/BestHumour")},
			{ GameCharacter.CharacterAttribute.Sorcery, GetNode<TextureRect>("BestWorstHBox/BestSorcery")}
		};
		_worstIcons = new Dictionary<GameCharacter.CharacterAttribute, TextureRect>()
		{
			{ GameCharacter.CharacterAttribute.Indemnity, GetNode<TextureRect>("BestWorstHBox/WorstIndemnity")},
			{ GameCharacter.CharacterAttribute.Resistance, GetNode<TextureRect>("BestWorstHBox/WorstResistance")},
			{ GameCharacter.CharacterAttribute.Solemnity, GetNode<TextureRect>("BestWorstHBox/WorstSolemnity")}
		};
	}

	public void SetBestWorstIcons(GameCharacter.CharacterAttribute bestAttribute, GameCharacter.CharacterAttribute worstAttribute)
	{
		if (! HasNode("BestWorstHBox"))
		{
			GD.Print("BestWorstHBox does not exist as a child of this node: " + Name);
			return;
		}
		foreach (TextureRect icon in _bestIcons.Values)
		{
			icon.Visible = false;
		}
		foreach (TextureRect icon in _worstIcons.Values)
		{
			icon.Visible = false;
		}
		_bestIcons[bestAttribute].Visible = true;
		_worstIcons[worstAttribute].Visible = true;
	}

	public void SetStatusEffects(List<StatusEffect> statusEffects)
	{
		foreach (TextureRect icon in _statusIcons.Values)
		{
			icon.Visible = false;
		}
		foreach (StatusEffect status in statusEffects)
		{
			_statusIcons[status].Visible = true;
		}
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
