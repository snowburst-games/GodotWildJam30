// making multiple speed heart sounds:
// get a heart rate of 60
// do heart rate divided by 60 to get the speed
// do 1 / the speed to get the target length of the audio file
// multiply 0.38 by the new length of the audio file to get where to move the 2nd heart sound
// e.g.:
// 80/60 = 1.33
// 1 / 1.33 = 0.75 (resize the audio file to cut off at 0.75)
// 0.38*0.75 = 0.2475 (move the 2nd heart sound to 0.2475)

using Godot;
using System;
using System.Collections.Generic;

public class BadHeart : Node2D
{
	[Export]
	private int _maxHeartRate = 200;

	private int _heartRate;

	public int AverageHeartRate {get; set;} = 60;

	private const int MINIMUM_HEARTRATE = 60;

	private int variability = 5;
	private bool _lost = false;


	private Random _rand = new Random();

	private Dictionary<AudioData.HeartSound, AudioStream> _heartSounds = AudioData.LoadedSounds<AudioData.HeartSound>(AudioData.HeartSoundPaths);

	[Signal]
	public delegate void MaxHeartRateExceeded();

	[Signal]
	public delegate void HeartRateChanged(int heartRate);


	private Label _lblRate;
	private AnimationPlayer _anim;
	private AudioStreamPlayer _soundPlayer;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_heartRate = AverageHeartRate;
		_lblRate = GetNode<Label>("LblRate");
		_anim = GetNode<AnimationPlayer>("Anim");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_lblRate.Text = _heartRate.ToString();

		
		AudioHandler.PlaySound(_soundPlayer, _heartSounds[AudioData.HeartSound.HeartSound60], AudioData.SoundBus.BadHeart);		
	}

	// public int GetFloorHeartRate()
	// {
	// 	int heartRateSound = 60;
	// 	foreach (int key in _heartSoundsByRate.Keys)
	// 	{
	// 		// if (key > _heartRate)
	// 		// {
	// 		// 	heartRateSound = key;
	// 		// }
	// 		if (_heartRate - key < 20 && _heartRate - key >= 0)
	// 		{
	// 			heartRateSound = key;
	// 		}
	// 	}
	// 	return heartRateSound;
	// }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		
	}

	public void OnHeartRateChanged(int modifier)
	{
		AverageHeartRate = Math.Max(MINIMUM_HEARTRATE, AverageHeartRate + modifier);
		_heartRate = AverageHeartRate;
		_lblRate.Text = _heartRate.ToString();

		_soundPlayer.PitchScale = _heartRate/60f;
		int busEffectsIndex = AudioServer.GetBusIndex(AudioData.SoundBusStrings[AudioData.SoundBus.BadHeart]);
		int numberofEffects = AudioServer.GetBusEffectCount(busEffectsIndex);
		for (int i = 0; i < numberofEffects; i++)
		{
			AudioServer.RemoveBusEffect(busEffectsIndex, i);
		}
		AudioEffectPitchShift audioEffect = new AudioEffectPitchShift();
		audioEffect.PitchScale = 1 / _soundPlayer.PitchScale;
		AudioServer.AddBusEffect(busEffectsIndex, audioEffect);

		EmitSignal(nameof(HeartRateChanged), AverageHeartRate);

	}
	private void OnSoundPlayerFinished()
	{
		// _soundPlayer.PitchScale = _heartRate/60f;
		if (_lost)
		{
			return;
		}
		// _soundPlayer.Play();
		// AudioHandler.PlaySound(_soundPlayer, _heartSounds[_heartSoundsByRate[GetFloorHeartRate()]], AudioData.SoundBus.Effects);
		AudioHandler.PlaySound(_soundPlayer, _heartSounds[AudioData.HeartSound.HeartSound60], AudioData.SoundBus.BadHeart);
		

		// _soundPlayer.PitchScale = _heartRate/60f;
		// int busEffectsIndex = AudioServer.GetBusIndex(AudioData.SoundBusStrings[AudioData.SoundBus.BadHeart]);
		// int numberofEffects = AudioServer.GetBusEffectCount(busEffectsIndex);
		// for (int i = 0; i < numberofEffects; i++)
		// {
		// 	AudioServer.RemoveBusEffect(busEffectsIndex, i);
		// }
		// AudioEffectPitchShift audioEffect = new AudioEffectPitchShift();
		// audioEffect.PitchScale = 1 / _soundPlayer.PitchScale;
		// AudioServer.AddBusEffect(busEffectsIndex, audioEffect);
		// GD.Print(GetFloorHeartRate());
	}

	private void OnVariabilityTimerTimeout()
	{
		_heartRate = Mathf.Clamp(_heartRate + _rand.Next(-variability, variability+1), AverageHeartRate-variability, AverageHeartRate+variability);
		_lblRate.Text = _heartRate.ToString();
		if (_heartRate > _maxHeartRate && !_lost)
		{
			_lost = true;
			EmitSignal(nameof(MaxHeartRateExceeded));
		}
		_anim.PlaybackSpeed = _heartRate/60f;

	}

}

