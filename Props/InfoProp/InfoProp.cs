using Godot;
using System;
using System.Collections.Generic;

public class InfoProp : TextureButton
{

	[Export]
	private string _infoText = "";

	[Export]
	private List<string> _infoTextList = new List<string>();

	[Export]
	private bool _searchable = false;
	[Export]
	private string _searchText = "Are you sure you want to go through the stress of searching this?";
	[Export]
	private bool _interactable = false;
	[Export]
	private bool _containsHeart = false;
	[Export]
	private int _heartRateModifier = 0;	
	[Export]
	private AudioData.InfoPropSound _soundOnSearch;

	private Panel _pnlFailed;
	// [Export]
	// private bool _playSoundOnLeftClick = false;

	[Signal]
	public delegate void HeartRateModified(int heartRateMod);

	[Signal]
	public delegate void HeartFound();
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.InfoPropSound, AudioStream> _infoPropSounds = AudioData.LoadedSounds<AudioData.InfoPropSound>(AudioData.InfoPropSoundPaths);

	// private Light2D _light;
	private InfoPanel _infoPanel;
	private Sprite _sprite;
	private Panel _pnlSearch;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		// _light = GetNode<Light2D>("Light2D");
		// _light.Visible = false;
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_infoPanel = GetNode<InfoPanel>("InfoPanel");
		_sprite = GetNode<Sprite>("Sprite");
		_pnlSearch = GetNode<Panel>("LayerSearch/PnlSearch");
		_pnlFailed = GetNode<Panel>("LayerSearch/PnlFailed");
		_pnlFailed.Visible = false;
		_pnlSearch.Visible = false;

		GetNode<Label>("LayerSearch/PnlSearch/LblSearch").Text = _searchText;

		if (_infoTextList.Count > 0)
		{
			_infoText = "";
			for (int i = 0; i < _infoTextList.Count; i++)
			{
				_infoText += _infoTextList[i];
				if (i != _infoTextList.Count-1)
				{
					_infoText += "\n";
				}
			}
		}

		_infoPanel.Init(_infoText);
	}

	private void OnMouseEntered()
	{
		// _light.Visible = true;
		Modulate = new Color(1.2f,1.2f,1.2f);
	}

	// private void PlaySoundOnSearch()
	// {
	// 	// if (!_playSoundOnLeftClick)
	// 	// {
	// 	// 	return;
	// 	// }
	// 	AudioHandler.PlaySound(_soundPlayer, _infoPropSounds[_soundOnSearch], AudioData.SoundBus.Effects);
	// }

	private void OnMouseExited()
	{
		// _light.Visible = false;
		Modulate = new Color(1,1,1);
	}
	private void OnBtnSpriteGuiInput(InputEvent ev)
	{
		if (ev is InputEventMouseButton evb)
		{
			if (evb.Pressed)
			{
				if (evb.ButtonIndex == (int)ButtonList.Right)
				{
					StartInfoPanel();
				}
				if (evb.ButtonIndex == (int)ButtonList.Left)
				{
					// PlaySoundOnSearch();
					if (_interactable)
					{
						return; // signal should be connected elsewhere
					}
					if (_searchable)
					{
						_pnlSearch.Visible = true;
					}
					else
					{
						StartInfoPanel();
					}
				}				
			}
			else
			{
				_infoPanel.Visible = false;
			}
		}
	}

	private void StartInfoPanel()
	{
		_infoPanel.Init(_infoText);
		_infoPanel.Start();
		_infoPanel.Visible = true;
	}

	private void OnBtnSearchPressed()
	{
		GetTree().Paused = true;
		AudioHandler.PlaySound(_soundPlayer, _infoPropSounds[_soundOnSearch], AudioData.SoundBus.Effects);
		if (_containsHeart)
		{
			GetTree().Paused = false;
			EmitSignal(nameof(HeartFound));
		}
		else
		{
			_pnlFailed.Visible = true;
			EmitSignal(nameof(HeartRateModified), _heartRateModifier);
		}
		_pnlSearch.Visible = false;
	}


	private void OnBtnOkayPressed()
	{
		_pnlFailed.Visible = false;
		GetTree().Paused = false;
	}

	private void OnBtnCancelPressed()
	{
		_pnlSearch.Visible = false;
	}


}


